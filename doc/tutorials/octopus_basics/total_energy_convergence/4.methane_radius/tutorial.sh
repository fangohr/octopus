#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

rm radius.dat
. ./radius.sh

gnuplot plot.gp

cp *.sh *.dat *.eps $OCTOPUS_TOP/doc/tutorials/octopus_basics/total_energy_convergence/4.methane_radius/
