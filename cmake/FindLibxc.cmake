#[==============================================================================================[
#                                  Libxc compatibility wrapper                                  #
]==============================================================================================]

# Upstream issues:
# https://gitlab.com/libxc/libxc/-/merge_requests/610
# https://gitlab.com/libxc/libxc/-/merge_requests/604

#[===[.md
# FindLibxc

Libxc compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

list(APPEND CMAKE_MESSAGE_CONTEXT FindLibxc)
include(Octopus)
# Try each Libxc major version before trying pkg-config
# We need to do this to ensure the targets are not populated
# https://gitlab.kitware.com/cmake/cmake/-/issues/25527#note_1479281
# https://discourse.cmake.org/t/write-basic-package-version-file-and-version-range/10021
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        # Skip libxc 5.0.0 specifically
        VERSIONS 7.0 6.0 5.1
        NAMES Libxc libxc
        PKG_MODULE_NAMES libxcf03)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(Libxc::xcf03 ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
elseif (NOT TARGET Libxc::xcf03 AND TARGET xcf03)
    add_library(Libxc::xcf03 ALIAS xcf03)
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
        URL https://gitlab.com/libxc/libxc
        DESCRIPTION "Library of exchange-correlation functionals for density-functional theory"
)

# Note that `VERISION_GREATER`, etc. treat `x` as `x.0.0`, `x.y` as `x.y.0`, etc.
if (Libxc_VERSION VERSION_GREATER_EQUAL 7)
    message(WARNING "Libxc version ${Libxc_VERSION} is not officially supported. Use it at your own risk.\n
	Please report any issues in the build and execution to the official repository so we can support it as quickly as possible.")
endif ()

list(POP_BACK CMAKE_MESSAGE_CONTEXT)
