#!/usr/bin/python3

SPECIAL_LIBRARIES = [
    'etsf-io',  # static library
    'sparskit',  # static library
    'berkeleygw',  # static library
    'blacs',  # for foss scalapack is used (name mismatch); Intel is probably different => ignore blacs
]

import re
import sys
import subprocess

assert len(sys.argv) == 3, 'usage: ./autotools_check_octopus_ldd.py "path/to/octopus-executable" "path/to/config.log"'


_, octopus, config_log_path = sys.argv
missing_library = False


linked_libs = subprocess.check_output(["ldd", octopus]).decode('utf-8')


with open(config_log_path) as f:
    for line in f:
        if not re.match(r"^\s+\$(.*?)configure", line):
            continue
        configure_flags = line
        break


for lib_match in re.finditer(r'with-(?P<lib>.*?)=', configure_flags):
    lib_name = lib_match.group('lib').replace('-prefix', '')
    if lib_name not in SPECIAL_LIBRARIES and not re.findall(rf'{lib_name}[^ ]*\.so', linked_libs):
        missing_library = True
        print(f"Could not find '{lib_name}' in octopus binary")


sys.exit(missing_library)
