Eigenvalues [eV]
 #st  Spin   Eigenvalue      Occupation
   1   --   -18.282738       2.000000
   2   --    -7.302364       1.000000
   3   --    -7.302364       1.000000
   4   --    -7.302364       1.000000

Energy [eV]:
      Total       =      -262.24162451
      Free        =      -262.24162451
      -----------
      Ion-ion     =         0.00000000
      Eigenvalues =       -58.47256640
      Hartree     =       221.90914809
      Int[n*v_xc] =       -76.98695216
      Exchange    =       -51.56037510
      Correlation =        -7.28648708
      vanderWaals =         0.00000000
      Delta XC    =         0.00000000
      Entropy     =         4.15888308
      -TS         =        -0.00000000
      Photon ex.  =         0.00000000
      Kinetic     =       170.64607646
      External    =      -595.94990797
      Non-local   =       -69.80187455
      Int[n*v_E]  =         0.00000000

