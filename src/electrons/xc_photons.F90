!! Copyright (C) 2022 I.-T Lu
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

!> @brief This module implements the "photon-free" electron-photon exchange-correlation functional.
!!
!! For further details see
!! - <a href="https://arxiv.org/abs/2402.09794">Electron-Photon Exchange-Correlation Approximation for QEDFT, I-Te Lu, et al. </a>
!! - <a href="https://www.pnas.org/content/118/41/e2110464118">Making ab initio QED functional(s):
!!      Nonperturbative and photon-free effective frameworks for strong light–matter coupling, C. Schaefer et al</a>

module xc_photons_oct_m
  use debug_oct_m
  use derivatives_oct_m
  use epot_oct_m
  use global_oct_m
  use grid_oct_m
  use kpoints_oct_m
  use lalg_basic_oct_m
  use mesh_oct_m
  use mesh_function_oct_m
  use messages_oct_m
  use namespace_oct_m
  use photon_mode_oct_m
  use poisson_oct_m
  use restart_oct_m
  use space_oct_m
  use states_elec_oct_m
  use parser_oct_m
  use profiling_oct_m
  use solvers_oct_m

  implicit none

  private
  public :: xc_photons_t

  !> @brief This class described the 'photon-exchange' electron-photon xc functionals, based on QEDFT
  !!
  !! Currently, two functionals are available:
  !! - LDA
  !! - wave function based functional
  !!
  !! The choice of the functional is controlled by the input variable
  !! <a href="https://octopus-code.org/documentation/main/variables/hamiltonian/xc/xcphotonfunctional/">XCPhotonFunctional</a>
  !
  type xc_photons_t
    private
    integer                      :: method = 0        !< 0 = no px, 1 = LDA, 2 = wave function
    FLOAT,   allocatable, public :: vpx(:)            !< photon-free px potential
    FLOAT,                public :: ex                !< photon-free px energy
    type(photon_mode_t)          :: pt
    FLOAT                        :: pxlda_kappa       !< the scaling factor for the px LDA
    FLOAT                        :: eta_c     !< the scaling factor for the px potential to
    !!                                                   reproduce the perturbation results
    integer                      :: energy_method = 0          !< 1 = virial, 2 = expectation (only for 1 electron)
    logical                      :: lcorrelations = .false.    !< use the factor to approximate correlations
    logical                      :: llamb_re_mass = .false.    !< .true. to renormalize the electron mass
    logical                      :: llamb_freespace =.false.   !< .true. to run the lamb shift in free space
    FLOAT                        :: lamb_omega                 !< the frequency cutoff for lamb shift

    logical,              public :: lpfmf = .false. !< if .true. include the photon-free mean field in vector potential
    FLOAT,   allocatable, public :: mf_vector_potential(:)
    FLOAT,   allocatable         :: jp_proj_eo(:,:) !< paramagnetic current projected to the polarization
    !!                                                 direction and decomposed into cosine (e:even)
    !!                                                 and sine (o: odd)

  contains
    procedure :: init => xc_photons_init
    procedure :: end => xc_photons_end
    procedure :: wants_to_renormalize_mass => xc_photons_wants_to_renormalize_mass
    procedure :: get_renormalized_mass => xc_photons_get_renormalized_emass
    procedure :: mf_dump => xc_photons_mf_dump
    procedure :: mf_load => xc_photons_mf_load
    procedure :: v_ks => xc_photons_v_ks
    procedure :: add_mean_field => xc_photons_add_mean_field
  end type xc_photons_t

  ! the PhotonXCXCMethod
  integer, private, parameter ::  &
    XC_PHOTONS_NONE    = 0,       &
    XC_PHOTONS_LDA     = 1,       &
    XC_PHOTONS_WFS     = 2

contains

  ! ---------------------------------------------------------
  !> initialize the photon-exchange functional
  !
  subroutine xc_photons_init(xc_photons, namespace, xc_photon, space, gr, st)
    class(xc_photons_t),     intent(out)   :: xc_photons
    type(namespace_t),       intent(in)    :: namespace
    integer,                 intent(in)    :: xc_photon
    class(space_t),          intent(in)    :: space
    type(grid_t),            intent(in)    :: gr
    type(states_elec_t),     intent(in)    :: st

    PUSH_SUB(xc_photons_init)

    xc_photons%lpfmf = .false.

    call messages_experimental("XCPhotonFunctional /= none")

    call photon_mode_init(xc_photons%pt, namespace, space%dim, .true.)
    call photon_mode_set_n_electrons(xc_photons%pt, st%qtot)

    select case(xc_photon)
    case(OPTION__XCPHOTONFUNCTIONAL__PHOTON_X_LDA)
      xc_photons%method    = XC_PHOTONS_LDA
      xc_photons%lcorrelations = .false.
    case(OPTION__XCPHOTONFUNCTIONAL__PHOTON_XC_LDA)
      xc_photons%method    = XC_PHOTONS_LDA
      xc_photons%lcorrelations = .true.
    case(OPTION__XCPHOTONFUNCTIONAL__PHOTON_X_WFN)
      xc_photons%method    = XC_PHOTONS_WFS
      xc_photons%lcorrelations = .false.
    case(OPTION__XCPHOTONFUNCTIONAL__PHOTON_XC_WFN)
      xc_photons%method    = XC_PHOTONS_WFS
      xc_photons%lcorrelations = .true.
    case default
      xc_photons%method    = XC_PHOTONS_NONE
      return
    end select


    if (xc_photons%method == XC_PHOTONS_LDA) then

      !%Variable PhotonXCLDAKappa
      !%Type float
      !%Default 1.0
      !%Section Hamiltonian::XC
      !%Description
      !% the scaling factor for px-LDA potential
      !%End
      call parse_variable(namespace, 'PhotonXCLDAKappa', M_ONE, xc_photons%pxlda_kappa)

    end if

    !%Variable PhotonXCEnergyMethod
    !%Type integer
    !%Default 1
    !%Section Hamiltonian::XC
    !%Description
    !% There are different ways to calculate the energy,
    !%Option virial    1
    !% (modified) virial approach</br>
    !% <math>
    !%   (E_{\rm{px}}^{\rm{virial}} = \frac{1}{2}\int d\mathbf{r}\ \mathbf{r}\cdot[
    !%   -\rho(\mathbf{r})\nabla v_{\rm{px}}(\mathbf{r})])
    !%  </math></br>
    !%Option expectation_value 2
    !% expectation value w.tr.t. the wave functions (valid only for 1 electron)</br>
    !% <math>
    !%   E_{\rm{px}}[\rho] = -\sum_{\alpha=1}^{M_{p}}\frac{\tilde{\lambda}_{\alpha}^{2}}{2\tilde{\omega}_{\alpha}^{2}}
    !%   \langle (\tilde{\mathbf{{\varepsilon}}}_{\alpha}\cdot\hat{\mathbf{J}}_{\rm{p}})\Phi[\rho]
    !%   | (\tilde{\mathbf{{\varepsilon}}}_{\alpha}\cdot\hat{\mathbf{J}}_{\rm{p}})\Phi[\rho] \rangle
    !% </math></br>
    !%  This option only works for the wave function based electron-photon functionals
    !%Option LDA             3
    !% energy from electron density</br>
    !% <math>
    !%   E_{\rm pxLDA}[\rho] = \frac{-2\pi^{2}}{(d+2)({2V_{d}})^{\frac{2}{d}}}
    !%   \sum_{\alpha=1}^{M_{p}}\frac{\tilde{\lambda}_{\alpha}^{2}}{\tilde{\omega}_{\alpha}^{2}}
    !%   \int d\mathbf{r}\ \rho^{\frac{2+d}{d}}(\mathbf{r})
    !% </math></br>
    !% This option only works with LDA electron-photon functionals.
    !%End

    call parse_variable(namespace, 'PhotonXCEnergyMethod', 1, xc_photons%energy_method)

    if( xc_photons%method == XC_PHOTONS_WFS .and. xc_photons%energy_method == OPTION__PHOTONXCENERGYMETHOD__LDA ) then
      message(1) = "Calculating the electron-photon energy from the LDA expression"
      message(2) = "is not implemented for wave function based electron-photon functionals"
      call messages_fatal(2, namespace=namespace)
    end if


    if (xc_photons%lcorrelations) then

      !%Variable PhotonXCEtaC
      !%Type float
      !%Default 1.0
      !%Section Hamiltonian::XC
      !%Description
      !% The scaling factor for the px potential to reduce the weak coupling perturbation regime
      !%End

      if (parse_is_defined(namespace, 'PhotonXCEtaC')) then
        call parse_variable(namespace, 'PhotonXCEtaC', M_ONE, xc_photons%eta_c)
      else
        message(1) = "Defining PhotonXCEtaC is required for photon functionals containing correlation."
        call messages_fatal(1, namespace=namespace)
      end if

    else

      xc_photons%eta_c = M_ONE

    end if

    ! This variable will keep vpx across iterations
    SAFE_ALLOCATE(xc_photons%vpx(1:gr%np_part))

    xc_photons%vpx = M_ZERO

    !%Variable PhotonXCLambShift
    !%Type logical
    !%Default .false.
    !%Section Hamiltonian::XC
    !%Description
    !% to deal with the photon free exchange potential for continuum mode in free space
    !%End

    call parse_variable(namespace, 'PhotonXCLambShift', .false., xc_photons%llamb_freespace)
    call messages_experimental("PhotonXCLambShift", namespace=namespace)

    if (xc_photons%llamb_freespace) then

      !%Variable PhotonXCLambShiftOmegaCutoff
      !%Type float
      !%Default 0.0
      !%Section Hamiltonian::XC
      !%Description
      !% the cutoff frequency (Ha) for Lamb shift
      !%End

      call parse_variable(namespace, 'PhotonXCLambShiftOmegaCutoff', M_ZERO, xc_photons%lamb_omega)

      !%Variable PhotonXCLambShiftRenormalizeMass
      !%Type logical
      !%Default .false.
      !%Section Hamiltonian::XC
      !%Description
      !% to deal with the photon free exchange potential for continuum mode in free space
      !%End

      call parse_variable(namespace, 'PhotonXCLambShiftRenormalizeMass', .false., xc_photons%llamb_re_mass)

    end if

    ! compute the dressed photon modes
    call photon_mode_dressed(xc_photons%pt)


    POP_SUB(xc_photons_init)

  end subroutine xc_photons_init

  ! ---------------------------------------------------------
  subroutine xc_photons_end(this)
    class(xc_photons_t), intent(inout) :: this

    PUSH_SUB(xc_photons_end)

    call photon_mode_end(this%pt)
    SAFE_DEALLOCATE_A(this%vpx)

    if (allocated(this%mf_vector_potential)) then
      SAFE_DEALLOCATE_A(this%mf_vector_potential)
    end if
    if (allocated(this%jp_proj_eo)) then
      SAFE_DEALLOCATE_A(this%jp_proj_eo)
    end if

    POP_SUB(xc_photons_end)
  end subroutine xc_photons_end

  !> @brief evaluate the KS potential and energy for the given functional
  !!
  !! This routine is a wrapper to the specific routines
  !! - photon_free_vpx_lda()
  !! - photon_free_vpx_wfc()
  !
  subroutine xc_photons_v_ks(xc_photons, namespace, total_density, density, gr, space, psolver, ep, st)
    class(xc_photons_t),              intent(inout) :: xc_photons
    type(namespace_t),                intent(in)    :: namespace
    FLOAT, pointer, contiguous,       intent(in)    :: total_density(:)
    FLOAT, allocatable,               intent(in)    :: density(:, :)
    class(grid_t),                    intent(in)    :: gr
    type(space_t),                    intent(in)    :: space
    type(poisson_t),                  intent(in)    :: psolver
    type(epot_t),                     intent(in)    :: ep
    type(states_elec_t),              intent(inout) :: st

    integer :: ia

    PUSH_SUB(xc_photons_v_ks)

    xc_photons%lpfmf = xc_photons%method > 0

    xc_photons%vpx = M_ZERO
    xc_photons%ex = M_ZERO

    if ( .not. allocated(xc_photons%mf_vector_potential) ) then
      SAFE_ALLOCATE(xc_photons%mf_vector_potential(1:space%dim))
      xc_photons%mf_vector_potential = M_ZERO
    end if
    if ( .not. allocated(xc_photons%jp_proj_eo)) then
      SAFE_ALLOCATE(xc_photons%jp_proj_eo(1:xc_photons%pt%nmodes,1:2))
      xc_photons%jp_proj_eo = M_ZERO
    end if


    select case(xc_photons%method)
    case(XC_PHOTONS_LDA)  ! LDA approximation for px potential
      call photon_free_vpx_lda(namespace, xc_photons, total_density, gr, space, psolver)
    case(XC_PHOTONS_WFS)  ! wave function approxmation for px potential
      call photon_free_vpx_wfc(namespace, xc_photons, total_density, gr, space, st)
    case(XC_PHOTONS_NONE) ! no photon-exchange potential
      call messages_write('Photon-free px potential is not computed', new_line = .true.)
      call messages_info()
    case default
      ASSERT(.false.)
    end select


    if (.not. xc_photons%llamb_freespace) then
      ! add the constant energy shift
      do ia = 1, xc_photons%pt%nmodes
        xc_photons%ex = xc_photons%ex + 0.5_real64 * (xc_photons%pt%dressed_omega(ia)-xc_photons%pt%omega(ia))
      end do
    end if

    POP_SUB(xc_photons_v_ks)
  end subroutine xc_photons_v_ks
  ! ---------------------------------------------------------

  ! ---------------------------------------------------------
  ! ---------------------------------------------------------
  !
  !> @brief compute the electron-photon exchange potential within the LDA
  !!
  !! This is based on the expression
  !! \f[
  !!     \nabla^{2} v_{{\rm{pxLDA}}}(\mathbf{r}) =
  !!     -\sum_{\alpha=1}^{M_{p}} \frac{2 \pi^{2}\tilde{\lambda}_{\alpha}^{2}}{\tilde{\omega}_{\alpha}^{2}}
  !!      \left[
  !!         (\tilde{\mathbf{{\varepsilon}}}_{\alpha}\cdot\nabla)^{2}\left(\frac{\rho(\mathbf{r})}{2V_{d}}\right)^{\frac{2}{d}}
  !!      \right]
  !! \f]
  !!
  !! The calculation if the energy is determined by the variable
  !! <a href="https://octopus-code.org/documentation/main/variables/hamiltonian/xc/PhotonXCenergymethod/">PhotonXCEnergyMethod</a>:
  !! - `virial`:
  !! \f[
  !!   E_{\rm{px}}^{\rm{virial}} = \frac{1}{2}\int d\mathbf{r}\ \mathbf{r}\cdot[-\rho(\mathbf{r})\nabla v_{\rm{px}}(\mathbf{r})]
  !! \f]
  !! - `expectation_value`:
  !! \f[
  !!    E_{\rm{px}}[\rho] =
  !!      -\sum_{\alpha=1}^{M_{p}} \frac{\tilde{\lambda}_{\alpha}^{2}}{2\tilde{\omega}_{\alpha}^{2}}
  !!      \langle (\tilde{\mathbf{{\varepsilon}}}_{\alpha}\cdot\hat{\mathbf{J}}_{\rm{p}})\Phi[\rho] |
  !!      (\tilde{\mathbf{{\varepsilon}}}_{\alpha}\cdot\hat{\mathbf{J}}_{\rm{p}})\Phi[\rho] \rangle
  !! \f]
  !! - `LDA`:
  !! \f[
  !!     E_{\rm pxLDA}[\rho]=
  !!       \frac{-2\pi^{2}}{(d+2)({2V_{d}})^{\frac{2}{d}}}
  !!       \sum_{\alpha=1}^{M_{p}} \frac{\tilde{\lambda}_{\alpha}^{2}}{\tilde{\omega}_{\alpha}^{2}}
  !!       \int d\mathbf{r}\ \rho^{\frac{2+d}{d}}(\mathbf{r})
  !! \f]
  ! The Lamb shift code is experimental and untested
  subroutine photon_free_vpx_lda(namespace, xc_photons, total_density, gr, space, psolver)
    type(namespace_t),                intent(in)    :: namespace
    type(xc_photons_t),               intent(inout) :: xc_photons
    FLOAT, pointer, contiguous,       intent(in)    :: total_density(:)
    class(grid_t),                    intent(in)    :: gr
    type(space_t),                    intent(in)    :: space
    type(poisson_t),                  intent(in)    :: psolver

    integer :: ia, ip, iter
    FLOAT :: unit_volume, r, res,  presum, prefact
    FLOAT :: xx(space%dim), prefactor_lamb
    FLOAT, allocatable :: prefactor(:)
    FLOAT, allocatable :: rho_aux(:)
    FLOAT, allocatable :: grad_rho_aux(:,:)
    FLOAT, allocatable :: px_source(:)
    FLOAT, allocatable :: tmp1(:)
    FLOAT, allocatable :: tmp2(:,:)
    FLOAT, allocatable :: tmp3(:)
    FLOAT, allocatable :: grad_vpx(:,:)
    FLOAT, allocatable :: epsgrad_epsgrad_rho_aux(:)
    FLOAT, allocatable :: epx_force_module(:)

    FLOAT, parameter :: threshold = 1e-7_real64

    PUSH_SUB(photon_free_vpx_lda)

    if (xc_photons%energy_method == 2 .and. xc_photons%pt%n_electrons >1) then
      call messages_not_implemented("expectation value for energy for pxLDA more than 1 electron", namespace=namespace)
    end if

    xc_photons%vpx = M_ZERO
    xc_photons%ex = M_ZERO

    SAFE_ALLOCATE(prefactor(1:xc_photons%pt%nmodes))
    prefactor = M_ZERO
    ! here we will use only one spin channel
    SAFE_ALLOCATE(rho_aux(1:gr%np_part))
    SAFE_ALLOCATE(grad_rho_aux(1:gr%np, 1:xc_photons%pt%dim))
    SAFE_ALLOCATE(px_source(1:gr%np_part))
    SAFE_ALLOCATE(tmp1(1:gr%np_part))
    SAFE_ALLOCATE(tmp2(1:gr%np, 1:xc_photons%pt%dim))
    SAFE_ALLOCATE(tmp3(1:gr%np_part))
    SAFE_ALLOCATE(grad_vpx(1:gr%np, 1:xc_photons%pt%dim))
    grad_vpx = M_ZERO
    SAFE_ALLOCATE(epx_force_module(1:gr%np_part))
    epx_force_module = M_ZERO

    select case(xc_photons%pt%dim)
    case(1)
      unit_volume = M_TWO
    case(2)
      unit_volume = M_PI
    case(3)
      unit_volume = M_FOUR*M_PI/M_THREE
    case default
      call messages_not_implemented("LDA px more than 3 dimension", namespace=namespace)
    end select

    !$omp parallel do
    do ip=1, gr%np
      rho_aux(ip) = ( abs(total_density(ip))/(M_TWO*unit_volume) )**(M_TWO/(xc_photons%pt%dim*M_ONE))
    end do
    !$omp end parallel do


    ! compute the electron-photon exchange potential

    if (xc_photons%llamb_freespace) then

      ! Note: The Lamb shift part is currently experimental and untested!
      ! compute the electron-photon exchange potential

      prefactor_lamb = -(8.0_real64*M_PI*M_THIRD) * xc_photons%lamb_omega / (P_C**3)

      !$OMP parallel do
      do ip=1,gr%np
        xc_photons%vpx(ip) = prefactor_lamb*rho_aux(ip)
      end do
      !$OMP end parallel do

    else

      do ia = 1, xc_photons%pt%nmodes
        prefactor(ia) = -M_TWO*(M_PI * xc_photons%pt%dressed_lambda(ia) / xc_photons%pt%dressed_omega(ia))**2
      end do

      select case(xc_photons%pt%dim)
      case(1)
        ! solve the pxLDA potential using the analytical form in 1D
        px_source = M_ZERO
        do ia = 1, xc_photons%pt%nmodes
          !$OMP parallel do
          do ip=1,gr%np_part
            px_source(ip) = px_source(ip) +  prefactor(ia)
          end do
          !$OMP end parallel do
        end do

        !$OMP parallel do
        do ip=1,gr%np
          xc_photons%vpx(ip) = px_source(ip)*rho_aux(ip)
        end do
        !$OMP end parallel do
      case(2)
        call get_px_source(px_source)
        ! for 2D we solve the Poisson equation using the conjugate gradient method
        mesh_aux => gr%der%mesh
        iter = 1000
        call dconjugate_gradients(gr%np, xc_photons%vpx(:), px_source, laplacian_op, dmf_dotp_aux, iter, res, threshold)
        write(message(1),'(a,i6,a)')  "Info: CG converged with ", iter, " iterations."
        write(message(2),'(a,e14.6)')    "Info: The residue is ", res
        call messages_info(2, namespace=namespace)

      case(3)
        ! for 3D we use thepoisson solver including the prefactor (-4*pi)
        ! therefore, we need to remove the factor in advance
        call get_px_source(px_source)

        call lalg_scal(gr%np, M_ONE/(-M_FOUR*M_PI), px_source)

        ! solve the Poisson equation
        call dpoisson_solve(psolver, namespace, xc_photons%vpx(:), px_source)
      case default
        ASSERT(.false.)
      end select

    end if

    ! scaling the potential
    call lalg_scal(gr%np, (xc_photons%eta_c * xc_photons%pxlda_kappa), xc_photons%vpx)

    ! compute electron-photon energy

    select case (xc_photons%energy_method)
    case(1) ! compute the epx energy from the virial relation

      do ia = 1, xc_photons%pt%nmodes

        ! compute the electron-photon force
        !$omp parallel do
        do ip = 1, gr%np
          epx_force_module(ip) = -prefactor(ia)*M_TWO*abs(total_density(ip))*rho_aux(ip)/(xc_photons%pt%dim*M_ONE+M_TWO)
        end do
        !$omp end parallel do

        call dderivatives_grad(gr%der, epx_force_module(:), tmp2)
        call lalg_gemv(gr%np, xc_photons%pt%dim, M_ONE, tmp2, xc_photons%pt%dressed_pol(1:xc_photons%pt%dim, ia), M_ZERO, tmp1)

        !$omp parallel do private(r, xx)
        do ip = 1, gr%np
          call mesh_r(gr, ip, r, coords=xx)
          tmp3(ip) = tmp1(ip)*dot_product(xx(1:xc_photons%pt%dim), xc_photons%pt%dressed_pol(1:xc_photons%pt%dim, ia))
        end do
        !$omp end parallel do

        xc_photons%ex = xc_photons%ex + M_HALF*dmf_integrate(gr, tmp3)
      end do

      xc_photons%ex = xc_photons%eta_c * xc_photons%pxlda_kappa * xc_photons%ex

    case(2) ! compute the energy as expetation value wrt to the wave functions

      rho_aux(1:gr%np) = sqrt(abs( total_density(1:gr%np)))

      SAFE_ALLOCATE(epsgrad_epsgrad_rho_aux(1:gr%np))

      call dderivatives_grad(gr%der, rho_aux(1:gr%np_part), grad_rho_aux)

      do ia = 1, xc_photons%pt%nmodes
        prefact = (xc_photons%pt%dressed_lambda(ia)**2) / (M_TWO*xc_photons%pt%dressed_omega(ia)**2)

        call lalg_gemv(gr%np, xc_photons%pt%dim, M_ONE, grad_rho_aux, xc_photons%pt%dressed_pol(:, ia), M_ZERO, tmp1)
        call dderivatives_grad(gr%der, tmp1, tmp2)
        call lalg_gemv(gr%np, xc_photons%pt%dim, M_ONE, tmp2, xc_photons%pt%dressed_pol(1:xc_photons%pt%dim, ia), M_ZERO, tmp1)

        !$OMP parallel do
        do ip=1, gr%np
          epsgrad_epsgrad_rho_aux(ip) = epsgrad_epsgrad_rho_aux(ip) + prefact*tmp1(ip)
        end do
        !$OMP end parallel do

      end do

      xc_photons%ex = xc_photons%eta_c * dmf_dotp(gr, rho_aux(1:gr%np), epsgrad_epsgrad_rho_aux(1:gr%np))

      SAFE_DEALLOCATE_A(epsgrad_epsgrad_rho_aux)

    case(3) ! integrate the aux electron density over the volume

      !$OMP parallel do
      do ip=1,gr%np
        tmp1(ip) = abs( total_density(ip))**((M_ONE*xc_photons%pt%dim+M_TWO)/(xc_photons%pt%dim*M_ONE))
      end do
      !$OMP end parallel do

      ! sum over the prefactors
      presum = M_ZERO
      do ia = 1, xc_photons%pt%nmodes
        presum = presum + prefactor(ia)
      end do
      presum = presum * (M_ONE/(M_TWO*unit_volume))**(M_TWO/(xc_photons%pt%dim*M_ONE)) / (xc_photons%pt%dim*M_ONE+M_TWO)
      presum = presum * xc_photons%eta_c * xc_photons%pxlda_kappa

      xc_photons%ex = xc_photons%eta_c * xc_photons%pxlda_kappa * presum * dmf_integrate(gr, tmp1)

    end select

    SAFE_DEALLOCATE_A(prefactor)
    SAFE_DEALLOCATE_A(rho_aux)
    SAFE_DEALLOCATE_A(grad_rho_aux)
    SAFE_DEALLOCATE_A(px_source)
    SAFE_DEALLOCATE_A(tmp1)
    SAFE_DEALLOCATE_A(tmp2)
    SAFE_DEALLOCATE_A(tmp3)
    SAFE_DEALLOCATE_A(grad_vpx)
    SAFE_DEALLOCATE_A(epx_force_module)

    POP_SUB(photon_free_vpx_lda)

  contains
    ! ---------------------------------------------------------
    !> Computes Hx = (\Laplacian) x
    subroutine laplacian_op(x, hx)
      FLOAT,            intent(in)    :: x(:)
      FLOAT, contiguous,intent(out)   :: Hx(:)

      FLOAT, allocatable :: tmpx(:)

      SAFE_ALLOCATE(tmpx(1:gr%np_part))
      call lalg_copy(gr%np, x, tmpx)
      call dderivatives_lapl(gr%der, tmpx, Hx)
      SAFE_DEALLOCATE_A(tmpx)

    end subroutine laplacian_op

    subroutine get_px_source(px_source)
      FLOAT, intent(out) :: px_source(:)

      call dderivatives_grad(gr%der, rho_aux(1:gr%np_part), grad_rho_aux)

      px_source = M_ZERO
      do ia = 1, xc_photons%pt%nmodes
        call lalg_gemv(gr%np, xc_photons%pt%dim, M_ONE, grad_rho_aux, xc_photons%pt%dressed_pol(:, ia), M_ZERO, tmp1)
        call dderivatives_grad(gr%der, tmp1, tmp2)
        call lalg_gemv(gr%np, xc_photons%pt%dim, M_ONE, tmp2, xc_photons%pt%dressed_pol(1:xc_photons%pt%dim, ia), M_ZERO, tmp1)
        call lalg_axpy(gr%np, prefactor(ia), tmp1, px_source)
      end do

    end subroutine get_px_source

  end subroutine photon_free_vpx_lda
  ! ---------------------------------------------------------

  ! ---------------------------------------------------------
  !
  !> @brief compute the electron-photon exchange potential based on wave functions
  !!
  !! @note This works only for one electron cases.
  !!
  !! This is based on the expression
  !! \f[
  !!     v_{\rm{px}}(\mathbf{r}) =
  !!      \sum_{\alpha=1}^{M_{p}}\frac{{\tilde{\lambda}}_{\alpha}^{2}}{2\tilde{\omega}_{\alpha}^{2}}
  !!      \frac{(\tilde{\mathbf{{\varepsilon}}}_{\alpha}\cdot\nabla)^{2}
  !!       \rho^{\frac{1}{2}}(\mathbf{r})}{\rho^{\frac{1}{2}}(\mathbf{r})}
  !! \f]
  !!
  !! The calculation if the energy is determined by the variable
  !! <a href="https://octopus-code.org/documentation/main/variables/hamiltonian/xc/PhotonXCenergymethod/">PhotonXCEnergyMethod</a>:
  !! - `virial`:
  !! \f[
  !!   E_{\rm{px}}^{\rm{virial}} = \frac{1}{2}\int d\mathbf{r}\ \mathbf{r}\cdot[-\rho(\mathbf{r})\nabla v_{\rm{px}}(\mathbf{r})]
  !! \f]
  !! - `expectation_value`:
  !! \f[
  !!    E_{\rm{px}}[\rho] =
  !!      -\sum_{\alpha=1}^{M_{p}} \frac{\tilde{\lambda}_{\alpha}^{2}}{2\tilde{\omega}_{\alpha}^{2}}
  !!      \langle (\tilde{\mathbf{{\varepsilon}}}_{\alpha}\cdot\hat{\mathbf{J}}_{\rm{p}})\Phi[\rho] |
  !!      (\tilde{\mathbf{{\varepsilon}}}_{\alpha}\cdot\hat{\mathbf{J}}_{\rm{p}})\Phi[\rho] \rangle
  !! \f]
  !
  ! The Lamb shift code is experimental and untested

  subroutine photon_free_vpx_wfc(namespace, xc_photons, total_density, gr, space, st)
    type(namespace_t),                intent(in)    :: namespace
    type(xc_photons_t),               intent(inout) :: xc_photons
    FLOAT, pointer, contiguous,       intent(in)    :: total_density(:)
    class(grid_t),                    intent(in)    :: gr
    type(space_t),                    intent(in)    :: space
    type(states_elec_t),              intent(in)    :: st

    integer :: ia, ip
    FLOAT :: prefactor_lamb
    FLOAT :: xx(space%dim), r
    FLOAT, allocatable :: prefactor(:)
    FLOAT, allocatable :: rho_aux(:)
    FLOAT, allocatable :: grad_rho_aux(:,:)
    FLOAT, allocatable :: grad_vpx(:,:)
    FLOAT, allocatable :: epsgrad_epsgrad_rho_aux(:)
    FLOAT, allocatable :: tmp1(:)
    FLOAT, allocatable :: tmp2(:,:)
    FLOAT::  shift

    PUSH_SUB(photon_free_vpx_wfc)

    if (st%d%nspin >1) then
      call messages_not_implemented("PhotonXCXCMethod = wavefunction for polarized and spinor cases", namespace=namespace)
    end if

    if (xc_photons%pt%n_electrons >1) then
      call messages_not_implemented("PhotonXCXCMethod = wavefunction for more than 1 electron", namespace=namespace)
    end if

    xc_photons%vpx = M_ZERO
    xc_photons%ex = M_ZERO

    SAFE_ALLOCATE(prefactor(1:xc_photons%pt%nmodes))
    prefactor = M_ZERO
    SAFE_ALLOCATE(rho_aux(1:gr%np_part))
    rho_aux = M_ZERO
    SAFE_ALLOCATE(grad_rho_aux(1:gr%np, 1:xc_photons%pt%dim))
    grad_rho_aux = M_ZERO
    SAFE_ALLOCATE(epsgrad_epsgrad_rho_aux(1:gr%np))
    SAFE_ALLOCATE(grad_vpx(1:gr%np, 1:xc_photons%pt%dim))
    SAFE_ALLOCATE(tmp1(1:gr%np_part))
    SAFE_ALLOCATE(tmp2(1:gr%np_part, 1:xc_photons%pt%dim))


    rho_aux(1:gr%np) = sqrt(abs( total_density(1:gr%np)))
    !$OMP parallel do
    do ip = 1, gr%np
      rho_aux(ip) = SAFE_TOL(rho_aux(ip),1e-18_real64)
    end do
    !$OMP end parallel do

    if (xc_photons%llamb_freespace) then

      ! experimental Lamb shift mode

      prefactor_lamb = ( M_TWO/(M_THREE*M_PI) ) * xc_photons%lamb_omega / P_C**3
      call dderivatives_lapl(gr%der, rho_aux(1:gr%np_part), epsgrad_epsgrad_rho_aux)
      call lalg_scal(gr%np, prefactor_lamb, epsgrad_epsgrad_rho_aux)

    else

      do ia = 1, xc_photons%pt%nmodes
        prefactor(ia) = (xc_photons%pt%dressed_lambda(ia)**2) / (M_TWO*xc_photons%pt%dressed_omega(ia)**2)
      end do

      call dderivatives_grad(gr%der, rho_aux, grad_rho_aux)

      epsgrad_epsgrad_rho_aux = M_ZERO
      do ia = 1, xc_photons%pt%nmodes
        tmp1 = M_ZERO
        call lalg_gemv(gr%np, xc_photons%pt%dim, M_ONE, grad_rho_aux, xc_photons%pt%dressed_pol(:, ia), M_ZERO, tmp1)
        call dderivatives_grad(gr%der, tmp1, tmp2)
        call lalg_gemv(gr%np, xc_photons%pt%dim, prefactor(ia), tmp2, xc_photons%pt%dressed_pol(:, ia), &
          M_ONE, epsgrad_epsgrad_rho_aux)
      end do

    end if

    !$OMP parallel do
    do ip = 1, gr%np
      xc_photons%vpx(ip) = epsgrad_epsgrad_rho_aux(ip)/rho_aux(ip)
    end do
    !$OMP end parallel do

    if(st%eigenval(1,1) < M_HUGE .and. .not. space%is_periodic()) then
      shift = M_TWO * st%eigenval(1,1) * prefactor(1)
      !$OMP parallel do
      do ip=1, gr%np
        xc_photons%vpx(ip) = xc_photons%vpx(ip) + shift
      end do
      !$OMP end parallel do
    end if

    call lalg_scal(gr%np, xc_photons%eta_c, xc_photons%vpx(:))

    select case (xc_photons%energy_method)
    case(1) ! virial

      call dderivatives_grad(gr%der, xc_photons%vpx(:), grad_vpx)
      !$OMP parallel do private(r, xx)
      do ip = 1, gr%np
        call mesh_r(gr, ip, r, coords=xx)
        tmp1(ip) = - total_density(ip)*dot_product(xx(1:xc_photons%pt%dim), grad_vpx(ip,1:xc_photons%pt%dim))
      end do
      !$OMP end parallel do

      xc_photons%ex = M_HALF*dmf_integrate(gr, tmp1)

    case(2) ! expectation_value
      xc_photons%ex = xc_photons%eta_c * dmf_dotp(gr, rho_aux(1:gr%np), epsgrad_epsgrad_rho_aux)

    case default
      ASSERT(.false.)
    end select

    SAFE_DEALLOCATE_A(prefactor)
    SAFE_DEALLOCATE_A(rho_aux)
    SAFE_DEALLOCATE_A(grad_rho_aux)
    SAFE_DEALLOCATE_A(grad_vpx)
    SAFE_DEALLOCATE_A(epsgrad_epsgrad_rho_aux)
    SAFE_DEALLOCATE_A(tmp1)
    SAFE_DEALLOCATE_A(tmp2)

    POP_SUB(photon_free_vpx_wfc)

  end subroutine photon_free_vpx_wfc
  ! ---------------------------------------------------------


  ! ---------------------------------------------------------
  !> @brief accumulate the results of time integral the paramagnetic current.
  !!
  !! This is based on the formula
  !! \f[
  !!     \tilde{A}_{\rm{s},\alpha}(t) =
  !!     -c\int_{-\infty}^{t} \frac{\tilde{\lambda}_{\alpha}^{2}}{\tilde{\omega}_{\alpha}}
  !!     \sin\left[\tilde{\omega}_{\alpha}(t-t_1)\right]
  !!     \tilde{\mathbf{{\varepsilon}}}_{\alpha} \cdot \mathbf{j}_{\rm{p}}(t_1) {dt_1}
  !! \f]
  !
  subroutine xc_photons_add_mean_field(xc_photons, gr, space, kpoints, st, time, dt)
    class(xc_photons_t),              intent(inout) :: xc_photons
    class(grid_t),                    intent(in)    :: gr
    class(space_t),                   intent(in)    :: space
    type(kpoints_t),                  intent(in)    :: kpoints
    type(states_elec_t),              intent(inout) :: st
    FLOAT,                            intent(in)    :: time
    FLOAT,                            intent(in)    :: dt


    integer :: ia, idir, ispin
    FLOAT   :: pol_dot_jp
    FLOAT, allocatable :: current(:,:,:)
    FLOAT, allocatable :: jp(:)

    PUSH_SUB(xc_photons_add_mean_field)

    ! compute the dressed photon-free vector potential
    xc_photons%mf_vector_potential = M_ZERO

    SAFE_ALLOCATE(current(1:gr%np_part, 1:space%dim, 1:st%d%nspin))
    current = M_ZERO
    ! here we use the paramagnetic current; note that the physical current here
    ! only contains the paramagnetic current.
    call states_elec_calc_quantities(gr, st, kpoints, .false., paramagnetic_current = current)

    ! compute the paramagnetic current
    SAFE_ALLOCATE(jp(1:space%dim))
    do idir = 1, space%dim
      jp(idir) = M_ZERO
      do ispin = 1, st%d%spin_channels
        jp(idir) = jp(idir) + dmf_integrate(gr%der%mesh, current(:,idir,ispin))
      end do
    end do

    ! update the 'projected' current
    do ia = 1, xc_photons%pt%nmodes
      pol_dot_jp = dot_product(xc_photons%pt%dressed_pol(1:space%dim, ia),jp(1:space%dim))
      xc_photons%jp_proj_eo(ia,1) = xc_photons%jp_proj_eo(ia,1) + &
        cos(xc_photons%pt%dressed_omega(ia)*( time-dt))*pol_dot_jp*dt
      xc_photons%jp_proj_eo(ia,2) = xc_photons%jp_proj_eo(ia,2) + &
        sin(xc_photons%pt%dressed_omega(ia)*( time-dt))*pol_dot_jp*dt
    end do

    do ia = 1, xc_photons%pt%nmodes
      xc_photons%mf_vector_potential(1:xc_photons%pt%dim) = xc_photons%mf_vector_potential(1:xc_photons%pt%dim) &
        + (-P_C*(xc_photons%pt%dressed_lambda(ia)**2) / xc_photons%pt%dressed_omega(ia)) &
        * (xc_photons%jp_proj_eo(ia,1)*sin(xc_photons%pt%dressed_omega(ia)* time) &
        - xc_photons%jp_proj_eo(ia,2)*cos(xc_photons%pt%dressed_omega(ia)* time)) &
        * xc_photons%pt%dressed_pol(1:xc_photons%pt%dim, ia)
    end do

    SAFE_DEALLOCATE_A(current)
    SAFE_DEALLOCATE_A(jp)

    POP_SUB(xc_photons_add_mean_field)

  end subroutine xc_photons_add_mean_field
  ! ---------------------------------------------------------


  !> indicate whether the photon-exchange requires a renormalized electron mass
  !!
  !! Renormalizing the electron mass is required if using the free-space Lamb shift.
  !
  logical pure function xc_photons_wants_to_renormalize_mass(xc_photons) result (renorm)
    class(xc_photons_t), intent(in) :: xc_photons

    renorm = (xc_photons%method>0) .and. xc_photons%llamb_freespace .and. xc_photons%llamb_re_mass

  end function xc_photons_wants_to_renormalize_mass

  !> return the renormalized electron mass for the electron-photon exhange
  FLOAT pure function xc_photons_get_renormalized_emass(xc_photons) result(mass)
    class(xc_photons_t), intent(in) :: xc_photons

    mass = M_ONE - (M_FOUR*xc_photons%lamb_omega) / (3.0*M_PI * P_C**3)

  end function xc_photons_get_renormalized_emass

  !> write restart information
  !
  subroutine xc_photons_mf_dump(xc_photons, restart, ierr)
    class(xc_photons_t),      intent(in)  :: xc_photons
    type(restart_t),          intent(in)  :: restart
    integer,                  intent(out) :: ierr

    character(len=80), allocatable :: lines(:)
    integer :: iunit, err, jj, nmodes
    integer :: pt_dim

    PUSH_SUB(photon_free_mf_dump)
    nmodes = xc_photons%pt%nmodes
    pt_dim = xc_photons%pt%dim

    SAFE_ALLOCATE(lines(1:nmodes+pt_dim))

    ierr = 0

    iunit = restart_open(restart, 'photon_free_mf')

    do jj = 1, pt_dim
      write(lines(jj), '(2x, es19.12)') xc_photons%mf_vector_potential(jj)
    end do

    do jj = 1, nmodes
      write(lines(jj+pt_dim), '(a10,1x,I8,a1,2x,2(es19.12,2x))') 'Mode ', jj, ":", xc_photons%jp_proj_eo(jj,1:2)
    end do

    call restart_write(restart, iunit, lines, nmodes+pt_dim, err)
    if (err /= 0) ierr = ierr + 1
    call restart_close(restart, iunit)

    SAFE_DEALLOCATE_A(lines)

    POP_SUB(photon_free_mf_dump)
  end subroutine xc_photons_mf_dump

! ---------------------------------------------------------

  !> load restart information
  !
  subroutine xc_photons_mf_load(xc_photons, restart, space, ierr)
    class(xc_photons_t),      intent(inout) :: xc_photons
    type(restart_t),          intent(in)    :: restart
    class(space_t),           intent(in)    :: space
    integer,                  intent(out)   :: ierr

    character(len=80), allocatable :: lines(:)
    character(len=7) :: sdummy
    integer :: idummy
    integer :: iunit, err, jj, nmodes
    integer :: pt_dim

    PUSH_SUB(photon_free_mf_load)

    ierr = 0
    nmodes = xc_photons%pt%nmodes
    pt_dim = xc_photons%pt%dim

    if (restart_skip(restart)) then
      ierr = -1
      POP_SUB(photon_free_mf_load)
      return
    end if

    if (debug%info) then
      message(1) = "Debug: Reading Photon-Free Photons restart."
      call messages_info(1, namespace=restart%namespace)
    end if

    if ( .not. allocated(xc_photons%jp_proj_eo)) then
      SAFE_ALLOCATE(xc_photons%jp_proj_eo(1:xc_photons%pt%nmodes, 1:2))
      xc_photons%jp_proj_eo = M_ZERO
    end if
    if ( .not. allocated(xc_photons%mf_vector_potential)) then
      SAFE_ALLOCATE(xc_photons%mf_vector_potential(1:space%dim))
      xc_photons%mf_vector_potential = M_ZERO
    end if

    SAFE_ALLOCATE(lines(1:nmodes+pt_dim))
    iunit = restart_open(restart, 'photon_free_mf')
    call restart_read(restart, iunit, lines, nmodes+pt_dim, err)
    if (err /= 0) then
      ierr = ierr + 1
    else
      do jj = 1, pt_dim
        read(lines(jj),'(2x, es19.12)') xc_photons%mf_vector_potential(jj)
      end do

      do jj = 1, nmodes
        read(lines(jj+pt_dim), '(a10,1x,I8,a1,2x,2(es19.12,2x))') sdummy, idummy, sdummy, xc_photons%jp_proj_eo(jj,1:2)
      end do
    end if
    call restart_close(restart, iunit)

    if (debug%info) then
      message(1) = "Debug: Reading Photons restart done."
      call messages_info(1, namespace=restart%namespace)
    end if

    SAFE_DEALLOCATE_A(lines)

    POP_SUB(photon_free_mf_load)
  end subroutine xc_photons_mf_load

end module xc_photons_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
