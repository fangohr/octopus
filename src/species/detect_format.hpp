#ifndef PSEUDO_DETECT_FORMAT_HPP
#define PSEUDO_DETECT_FORMAT_HPP

/*
 Copyright (C) 2018 Xavier Andrade

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "base.hpp"
#include <fstream>
#include <iostream>
#include <rapidxml.hpp>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <vector>

namespace pseudopotential {

static pseudopotential::format detect_format(const std::string &filename) {

  // check that the file is not a directory
  struct stat file_stat;
  if (stat(filename.c_str(), &file_stat) == -1)
    return pseudopotential::format::FILE_NOT_FOUND;
  if (S_ISDIR(file_stat.st_mode))
    return pseudopotential::format::FILE_NOT_FOUND;

  // now open the file
  std::ifstream file(filename.c_str());

  if (!file)
    return pseudopotential::format::FILE_NOT_FOUND;

  // First, try to read the file extension from the filename. This will detect non-xml files
  std::string extension =  filename.substr(filename.find_last_of(".") + 1);
  std::transform(extension.begin(), extension.end(), extension.begin(), ::tolower);

  if (extension == "psp8" || extension == "drh")
    return pseudopotential::format::PSP8;
  if (extension == "psf")
    return pseudopotential::format::PSF;
  if (extension == "cpi")
    return pseudopotential::format::CPI;
  if (extension == "fhi")
    return pseudopotential::format::FHI;
  if (extension == "hgh")
    return pseudopotential::format::HGH;

  // If the file extension is not recognized, try to parse parse it as XML
  std::vector<char> buffer((std::istreambuf_iterator<char>(file)),
                           std::istreambuf_iterator<char>());
  buffer.push_back('\0');

  rapidxml::xml_document<> doc;
  try {
    doc.parse<0>(&buffer[0]);
    // Return the specific format if the file is recognized
    if (doc.first_node("fpmd:species"))
      return pseudopotential::format::QSO;
    if (doc.first_node("qbox:species"))
      return pseudopotential::format::QSO;
    if (doc.first_node("PP_INFO"))
      return pseudopotential::format::UPF1;
    if (doc.first_node("UPF"))
      return pseudopotential::format::UPF2;
    if (doc.first_node("psml"))
      return pseudopotential::format::PSML;
  } catch (rapidxml::parse_error xml_error) {
    // Currently Octopus is passing a folder name to the ps_init() routine, which calls this function.
    // Here, we loop over all files in that folder, and try to detect their type.
    // As these folders also contain files, such as Makefile, etc. the catch condition will always be
    // met, and Octopus would print the confusing error messages. Therefore, the error messages are
    // disabled, until the whole mechanism is better understood.

    // std::cerr << "Error parsing pseudopotential file, " << filename.c_str() << ", as XML." << std::endl;
    // std::cerr << "rapidxml error:" << xml_error.what() << std::endl;
  }

  return pseudopotential::format::UNKNOWN;
}

} // namespace pseudopotential

#endif
