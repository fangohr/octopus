!! Copyright (C) 2015 X. Andrade, R. A. DiStasio Jr., B. Santra
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

! This code is based on the Quantum Espresso implementation of TS.

#include "global.h"

module vdw_ts_oct_m
  use debug_oct_m
  use global_oct_m
  use hirshfeld_oct_m
  use io_oct_m
  use io_function_oct_m
  use ions_oct_m
  use, intrinsic :: iso_fortran_env
  use lattice_vectors_oct_m
  use lalg_basic_oct_m
  use messages_oct_m
  use mesh_oct_m
  use mpi_oct_m
  use namespace_oct_m
  use parser_oct_m
  use profiling_oct_m
  use ps_oct_m
  use space_oct_m
  use species_oct_m
  use unit_oct_m
  use unit_system_oct_m

  implicit none

  private

  public ::                               &
    vdw_ts_t,                             &
    vdw_ts_init,                          &
    vdw_ts_end,                           &
    vdw_ts_write_c6ab,                    &
    vdw_ts_force_calculate,               &
    vdw_ts_calculate

  type vdw_ts_t
    private
    FLOAT, allocatable :: c6free(:)        !< Free atomic volumes for each atomic species.
    FLOAT, allocatable :: dpfree(:)        !< Free atomic static dipole polarizability for each atomic species.
    FLOAT, allocatable :: r0free(:)        !< Free atomic vdW radius for each atomic species.
    FLOAT, allocatable :: c6abfree(:, :)   !< Free atomic heteronuclear C6 coefficient for each atom pair.
    FLOAT, allocatable :: volfree(:)
    FLOAT, allocatable :: c6ab(:, :)       !< Effective atomic heteronuclear C6 coefficient for each atom pair.
    FLOAT              :: cutoff           !< Cutoff value for the calculation of the VdW TS correction in periodic system.
    FLOAT              :: damping          !< Parameter for the damping function steepness.
    FLOAT              :: sr               !< Parameter for the damping function. Can depend on the XC correction used.

    FLOAT, allocatable :: derivative_coeff(:) !< A pre-calculated coefficient for fast derivative evaluation
  end type vdw_ts_t

contains

  subroutine vdw_ts_init(this, namespace, ions)
    type(vdw_ts_t),      intent(out)   :: this
    type(namespace_t),   intent(in)    :: namespace
    type(ions_t),        intent(in)    :: ions

    integer :: ispecies, jspecies
    FLOAT :: num, den

    PUSH_SUB(vdw_ts_init)

    !%Variable VDW_TS_cutoff
    !%Type float
    !%Default 10.0
    !%Section Hamiltonian::XC
    !%Description
    !% Set the value of the cutoff (unit of length) for the VDW correction in periodic system
    !% in the Tkatchenko and Scheffler (vdw_ts) scheme only.
    !%End
    call parse_variable(namespace, 'VDW_TS_cutoff', 10.0_real64, this%cutoff, units_inp%length)


    !%Variable VDW_TS_damping
    !%Type float
    !%Default 20.0
    !%Section Hamiltonian::XC
    !%Description
    !% Set the value of the damping function (in unit of 1/length) steepness for the VDW correction in the
    !% Tkatchenko-Scheffler scheme. See Equation (12) of Phys. Rev. Lett. 102 073005 (2009).
    !%End
    call parse_variable(namespace, 'VDW_TS_damping', 20.0_real64, this%damping, units_inp%length**(-1))

    !%Variable VDW_TS_sr
    !%Type float
    !%Default 0.94
    !%Section Hamiltonian::XC
    !%Description
    !% Set the value of the sr parameter in the damping function of the VDW correction in the
    !% Tkatchenko-Scheffler scheme. See Equation (12) of Phys. Rev. Lett. 102 073005 (2009).
    !% This parameter depends on the xc functional used.
    !% The default value is 0.94, which holds for PBE. For PBE0, a value of 0.96 should be used.
    !%End
    call parse_variable(namespace, 'VDW_TS_sr', 0.94_real64, this%sr)


    SAFE_ALLOCATE(this%c6free(1:ions%nspecies))
    SAFE_ALLOCATE(this%dpfree(1:ions%nspecies))
    SAFE_ALLOCATE(this%r0free(1:ions%nspecies))
    SAFE_ALLOCATE(this%volfree(1:ions%nspecies))
    SAFE_ALLOCATE(this%c6abfree(1:ions%nspecies, 1:ions%nspecies))
    SAFE_ALLOCATE(this%c6ab(1:ions%natoms, 1:ions%natoms))
    SAFE_ALLOCATE(this%derivative_coeff(1:ions%natoms))

    do ispecies = 1, ions%nspecies
      call get_vdw_param(namespace, species_label(ions%species(ispecies)), &
        this%c6free(ispecies), this%dpfree(ispecies), this%r0free(ispecies))
      this%volfree(ispecies) = ps_density_volume(species_ps(ions%species(ispecies)), namespace)
    end do

    do ispecies = 1, ions%nspecies
      do jspecies = 1, ions%nspecies
        num = M_TWO*this%c6free(ispecies)*this%c6free(jspecies)
        den = (this%dpfree(jspecies)/this%dpfree(ispecies))*this%c6free(ispecies) &
          + (this%dpfree(ispecies)/this%dpfree(jspecies))*this%c6free(jspecies)
        this%c6abfree(ispecies, jspecies) = num/den
      end do
    end do
    POP_SUB(vdw_ts_init)
  end subroutine vdw_ts_init

  !------------------------------------------

  subroutine vdw_ts_end(this)
    type(vdw_ts_t), intent(inout) :: this

    PUSH_SUB(vdw_ts_end)

    SAFE_DEALLOCATE_A(this%c6free)
    SAFE_DEALLOCATE_A(this%dpfree)
    SAFE_DEALLOCATE_A(this%r0free)
    SAFE_DEALLOCATE_A(this%volfree)
    SAFE_DEALLOCATE_A(this%c6abfree)
    SAFE_DEALLOCATE_A(this%c6ab)
    SAFE_DEALLOCATE_A(this%derivative_coeff)

    POP_SUB(vdw_ts_end)
  end subroutine vdw_ts_end

  !------------------------------------------

  subroutine vdw_ts_calculate(this, namespace, ions, mesh, nspin, density, energy, potential, force)
    type(vdw_ts_t),      intent(inout) :: this
    type(namespace_t),   intent(in)    :: namespace
    type(ions_t),        intent(in)    :: ions
    class(mesh_t),       intent(in)    :: mesh
    integer,             intent(in)    :: nspin
    FLOAT,               intent(in)    :: density(:, :)
    FLOAT,               intent(out)   :: energy
    FLOAT,               intent(out)   :: potential(:)
    FLOAT,               intent(out)   :: force(:, :)

    interface
      subroutine f90_vdw_calculate(natoms, dd, sr, zatom, coordinates, vol_ratio, &
        energy, force, derivative_coeff)
        use, intrinsic :: iso_fortran_env
        implicit none
        integer, intent(in)  :: natoms
        real(real64), intent(in)  :: dd
        real(real64), intent(in)  :: sr
        integer, intent(in)  :: zatom
        real(real64), intent(in)  :: coordinates
        real(real64), intent(in)  :: vol_ratio
        real(real64), intent(out) :: energy
        real(real64), intent(out) :: force
        real(real64), intent(out) :: derivative_coeff
      end subroutine f90_vdw_calculate
    end interface

    type(lattice_iterator_t) :: latt_iter
    integer :: iatom, jatom, ispecies, jspecies, jcopy, ip
    FLOAT :: rr, rr6, dffdr0, ee, ff, dee, dffdrab, dffdvra, deabdvra
    FLOAT, allocatable :: coordinates(:,:), vol_ratio(:), dvadens(:), dvadrr(:), &
      dr0dvra(:), r0ab(:,:)
    type(hirshfeld_t) :: hirshfeld
    integer, allocatable :: zatom(:)
    FLOAT :: x_j(ions%space%dim)

    PUSH_SUB(vdw_ts_calculate)

    SAFE_ALLOCATE(vol_ratio(1:ions%natoms))
    SAFE_ALLOCATE(dvadens(1:mesh%np))
    SAFE_ALLOCATE(dvadrr(1:3))
    SAFE_ALLOCATE(dr0dvra(1:ions%natoms))

    energy=M_ZERO
    force(1:ions%space%dim, 1:ions%natoms) = M_ZERO
    this%derivative_coeff(1:ions%natoms) = M_ZERO
    call hirshfeld_init(hirshfeld, mesh, ions, nspin)

    do iatom = 1, ions%natoms
      call hirshfeld_volume_ratio(hirshfeld, iatom, density, vol_ratio(iatom))
    end do

    do iatom = 1, ions%natoms
      ispecies = species_index(ions%atom(iatom)%species)
      dr0dvra(iatom) = this%r0free(ispecies)/(M_THREE*(vol_ratio(iatom)**(M_TWO/M_THREE)))
      do jatom = 1, ions%natoms
        jspecies = species_index(ions%atom(jatom)%species)
        this%c6ab(iatom,jatom) = vol_ratio(iatom)*vol_ratio(jatom)*this%c6abfree(ispecies,jspecies) !this operation is done again inside the .c part for the non periodic case
      end do
    end do

    if (ions%space%is_periodic()) then ! periodic case
      SAFE_ALLOCATE(r0ab(1:ions%natoms,1:ions%natoms))

      !Precomputing some quantities
      do iatom = 1, ions%natoms
        ispecies = species_index(ions%atom(iatom)%species)
        do jatom = 1, ions%natoms
          jspecies = species_index(ions%atom(jatom)%species)

          r0ab(iatom,jatom) = (vol_ratio(iatom)**(M_ONE/M_THREE))*this%r0free(ispecies) &
            + (vol_ratio(jatom)**(M_ONE/M_THREE))*this%r0free(jspecies)
        end do
      end do

      latt_iter = lattice_iterator_t(ions%latt, this%cutoff)
      do jatom = 1, ions%natoms
        jspecies = species_index(ions%atom(jatom)%species)

        do jcopy = 1, latt_iter%n_cells ! one of the periodic copy is the initial atom
          x_j = ions%pos(:, jatom) + latt_iter%get(jcopy)

          do iatom = 1, ions%natoms
            ispecies = species_index(ions%atom(iatom)%species)
            rr =  norm2(x_j - ions%pos(:, iatom))
            rr6 = rr**6

            if (rr < 1.0e-10_real64) cycle !To avoid self interaction

            ee = exp(-this%damping * ((rr / (this%sr*r0ab(iatom, jatom))) - M_ONE))
            ff = M_ONE/(M_ONE + ee)
            dee = ee*ff**2

            !Calculate the derivative of the damping function with respect to the distance between atoms A and B.
            dffdrab = (this%damping/(this%sr*r0ab(iatom, jatom)))*dee
            !Calculate the derivative of the damping function with respect to the distance between the van der Waals radius.
            dffdr0 =  -this%damping*rr/(this%sr*r0ab(iatom, jatom)**2)*dee

            energy = energy - M_HALF*ff*this%c6ab(iatom, jatom)/rr6

            ! Derivative of the damping function with respecto to the volume ratio of atom A.
            dffdvra = dffdr0*dr0dvra(iatom)

            ! Calculation of the pair-wise partial energy derivative with respect to the volume ratio of atom A.
            deabdvra = (dffdvra*this%c6ab(iatom, jatom) + ff*vol_ratio(jatom)*this%c6abfree(ispecies, jspecies))/rr6

            this%derivative_coeff(iatom) = this%derivative_coeff(iatom) + deabdvra

          end do
        end do
      end do

      SAFE_DEALLOCATE_A(r0ab)
    else ! Non periodic case
      SAFE_ALLOCATE(coordinates(1:ions%space%dim, 1:ions%natoms))
      SAFE_ALLOCATE(zatom(1:ions%natoms))

      do iatom = 1, ions%natoms
        coordinates(:, iatom) = ions%pos(:, iatom)
        zatom(iatom) = int(species_z(ions%atom(iatom)%species))

      end do

      call f90_vdw_calculate(ions%natoms,  this%damping, this%sr, zatom(1), coordinates(1, 1), &
        vol_ratio(1), energy, force(1, 1), this%derivative_coeff(1))


      SAFE_DEALLOCATE_A(coordinates)
      SAFE_DEALLOCATE_A(zatom)
    end if

    ! Calculate the potential
    potential = M_ZERO
    do iatom = 1, ions%natoms
      call hirshfeld_density_derivative(hirshfeld, iatom, dvadens)
      call lalg_axpy(mesh%np, -this%derivative_coeff(iatom), dvadens, potential)
    end do

    if (debug%info) then
      call dio_function_output(1_8, "./", "vvdw", namespace, ions%space, mesh, potential, unit_one, ip)
    end if

    call hirshfeld_end(hirshfeld)

    SAFE_DEALLOCATE_A(vol_ratio)
    SAFE_DEALLOCATE_A(dvadens)
    SAFE_DEALLOCATE_A(dvadrr)
    SAFE_DEALLOCATE_A(dr0dvra)

    POP_SUB(vdw_ts_calculate)
  end subroutine vdw_ts_calculate



  !------------------------------------------
  subroutine vdw_ts_force_calculate(this, force_vdw, ions, mesh, nspin, density)
    type(vdw_ts_t),      intent(in)    :: this
    type(ions_t),        intent(in)    :: ions
    FLOAT,               intent(inout) :: force_vdw(1:ions%space%dim, 1:ions%natoms)
    class(mesh_t),       intent(in)    :: mesh
    integer,             intent(in)    :: nspin
    FLOAT,               intent(in)    :: density(:, :)

    type(hirshfeld_t) :: hirshfeld
    type(lattice_iterator_t) :: latt_iter

    integer :: iatom, jatom, ispecies, jspecies, jcopy
    FLOAT :: rr, rr6,  dffdr0, ee, ff, dee, dffdvra, deabdvra, deabdrab, x_j(ions%space%dim)
    FLOAT, allocatable ::  vol_ratio(:), dvadrr(:), dr0dvra(:), r0ab(:,:), derivative_coeff(:), c6ab(:,:)
    type(profile_t), save :: prof

    PUSH_SUB(vdw_ts_force_calculate)

    call profiling_in(prof, "FORCE_VDW_TS")

    SAFE_ALLOCATE(vol_ratio(1:ions%natoms))
    SAFE_ALLOCATE(dvadrr(1:3))
    SAFE_ALLOCATE(dr0dvra(1:ions%natoms))
    SAFE_ALLOCATE(r0ab(1:ions%natoms,1:ions%natoms))
    SAFE_ALLOCATE(derivative_coeff(1:ions%natoms))
    SAFE_ALLOCATE(c6ab(1:ions%natoms,1:ions%natoms))


    force_vdw(1:ions%space%dim, 1:ions%natoms) = M_ZERO
    derivative_coeff(1:ions%natoms) = M_ZERO
    c6ab(1:ions%natoms,1:ions%natoms) = M_ZERO
    r0ab(1:ions%natoms,1:ions%natoms) = M_ZERO
    dr0dvra(1:ions%natoms) = M_ZERO
    dvadrr(1:ions%space%dim) = M_ZERO
    vol_ratio(1:ions%natoms) = M_ZERO


    call hirshfeld_init(hirshfeld, mesh, ions, nspin)


    do iatom = 1, ions%natoms
      call hirshfeld_volume_ratio(hirshfeld, iatom, density, vol_ratio(iatom))
    end do

    do iatom = 1, ions%natoms
      ispecies = species_index(ions%atom(iatom)%species)
      dr0dvra(iatom) = this%r0free(ispecies)/(M_THREE*(vol_ratio(iatom)**(M_TWO/M_THREE)))
      do jatom = 1, ions%natoms
        jspecies = species_index(ions%atom(jatom)%species)
        c6ab(iatom, jatom) = vol_ratio(iatom)*vol_ratio(jatom)*this%c6abfree(ispecies, jspecies)
      end do
    end do

    !Precomputing some quantities
    do iatom = 1, ions%natoms
      ispecies = species_index(ions%atom(iatom)%species)
      do jatom = iatom, ions%natoms
        jspecies = species_index(ions%atom(jatom)%species)

        r0ab(iatom, jatom) = (vol_ratio(iatom)**(M_ONE/M_THREE))*this%r0free(ispecies) &
          + (vol_ratio(jatom)**(M_ONE/M_THREE))*this%r0free(jspecies)
        if (iatom /= jatom) r0ab(jatom, iatom) = r0ab(iatom, jatom)
      end do
    end do

    latt_iter = lattice_iterator_t(ions%latt, this%cutoff)
    do jatom = 1, ions%natoms
      jspecies = species_index(ions%atom(jatom)%species)

      do jcopy = 1, latt_iter%n_cells ! one of the periodic copy is the initial atom
        x_j = ions%pos(:, jatom) + latt_iter%get(jcopy)
        do iatom = 1, ions%natoms
          ispecies = species_index(ions%atom(iatom)%species)
          rr =  norm2(x_j - ions%pos(:, iatom))
          rr6 = rr**6

          if (rr < TOL_HIRSHFELD) cycle !To avoid self interaction

          ee = exp(-this%damping*(rr/(this%sr*r0ab(iatom, jatom)) - M_ONE))
          ff = M_ONE/(M_ONE + ee)
          dee = ee*ff**2
          !Calculate the derivative of the damping function with respect to the van der Waals radius.
          dffdr0 =  -this%damping*rr/( this%sr*r0ab(iatom, jatom)**2)*dee
          ! Calculation of the pair-wise partial energy derivative with respect to the distance between atoms A and B.
          deabdrab = c6ab(iatom,jatom)*(this%damping/(this%sr*r0ab(iatom, jatom))*dee - 6.0_real64*ff/rr)/rr6
          ! Derivative of the damping function with respecto to the volume ratio of atom A.
          dffdvra = dffdr0*dr0dvra(iatom)
          ! Calculation of the pair-wise partial energy derivative with respect to the volume ratio of atom A.
          deabdvra = (dffdvra*c6ab(iatom, jatom) + ff*vol_ratio(jatom)*this%c6abfree(ispecies, jspecies))/rr6
          !Summing for using later
          derivative_coeff(iatom) = derivative_coeff(iatom) + deabdvra

          ! Calculation of the pair-wise partial energy derivative with respect to the distance between atoms A and B.
          deabdrab = c6ab(iatom, jatom)*(this%damping/(this%sr*r0ab(iatom, jatom))*dee - 6.0_real64*ff/rr)/rr6
          force_vdw(:, iatom) = force_vdw(:, iatom) + M_HALF*deabdrab*(ions%pos(:, iatom) - x_j)/rr
        end do
      end do
    end do

    do iatom = 1, ions%natoms
      do jatom = 1, ions%natoms
        call hirshfeld_position_derivative(hirshfeld, iatom, jatom, density, dvadrr) !dvadrr_ij = \frac{\delta V_i}{\delta \vec{x_j}}
        force_vdw(:, jatom)= force_vdw(:, jatom) + derivative_coeff(iatom)*dvadrr  ! ions%atom(jatom)%f_vdw = sum_i coeff_i * dvadrr_ij
      end do
    end do

    call hirshfeld_end(hirshfeld)

    SAFE_DEALLOCATE_A(vol_ratio)
    SAFE_DEALLOCATE_A(dvadrr)
    SAFE_DEALLOCATE_A(dr0dvra)
    SAFE_DEALLOCATE_A(r0ab)
    SAFE_DEALLOCATE_A(derivative_coeff)
    SAFE_DEALLOCATE_A(c6ab)

    call profiling_out(prof)

    POP_SUB(vdw_ts_force_calculate)
  end subroutine vdw_ts_force_calculate

  !------------------------------------------

  subroutine vdw_ts_write_c6ab(this, ions, dir, fname, namespace)
    type(vdw_ts_t)  , intent(inout) :: this
    type(ions_t),        intent(in) :: ions
    character(len=*), intent(in)    :: dir, fname
    type(namespace_t),   intent(in) :: namespace

    integer :: iunit, iatom, jatom

    PUSH_SUB(vdw_ts_write_c6ab)

    if (mpi_grp_is_root(mpi_world)) then
      call io_mkdir(dir, namespace)
      iunit = io_open(trim(dir) // "/" // trim(fname), namespace, action='write')
      write(iunit, '(a)') ' # Atom1 Atom2 C6_{12}^{eff}'


      do iatom = 1, ions%natoms
        do jatom = 1, ions%natoms
          write(iunit, '(3x, i5, i5, e15.6)') iatom, jatom, this%c6ab(iatom, jatom)
        end do
      end do
      call io_close(iunit)
    end if
    POP_SUB(vdw_ts_write_c6ab)

  end subroutine vdw_ts_write_c6ab




  !------------------------------------------

  subroutine get_vdw_param(namespace, atom, c6, alpha, r0)
    type(namespace_t), intent(in)  :: namespace
    character(len=*),  intent(in)  :: atom
    FLOAT,             intent(out) :: c6
    FLOAT,             intent(out) :: alpha
    FLOAT,             intent(out) :: r0

    PUSH_SUB(get_vdw_param)

    select case (trim(atom))

    case ('H')
      alpha = 4.500000_real64
      c6 = 6.500000_real64
      r0 = 3.100000_real64

    case ('He')
      alpha = 1.380000_real64
      c6 = 1.460000_real64
      r0 = 2.650000_real64

    case ('Li')
      alpha = 164.200000_real64
      c6 = 1387.000000_real64
      r0 = 4.160000_real64

    case ('Be')
      alpha = 38.000000_real64
      c6 = 214.000000_real64
      r0 = 4.170000_real64

    case ('B')
      alpha = 21.000000_real64
      c6 = 99.500000_real64
      r0 = 3.890000_real64

    case ('C')
      alpha = 12.000000_real64
      c6 = 46.600000_real64
      r0 = 3.590000_real64

    case ('N')
      alpha = 7.400000_real64
      c6 = 24.200000_real64
      r0 = 3.340000_real64

    case ('O')
      alpha = 5.400000_real64
      c6 = 15.600000_real64
      r0 = 3.190000_real64

    case ('F')
      alpha = 3.800000_real64
      c6 = 9.520000_real64
      r0 = 3.040000_real64

    case ('Ne')
      alpha = 2.670000_real64
      c6 = 6.380000_real64
      r0 = 2.910000_real64

    case ('Na')
      alpha = 162.700000_real64
      c6 = 1556.000000_real64
      r0 = 3.730000_real64

    case ('Mg')
      alpha = 71.000000_real64
      c6 = 627.000000_real64
      r0 = 4.270000_real64

    case ('Al')
      alpha = 60.000000_real64
      c6 = 528.000000_real64
      r0 = 4.330000_real64

    case ('Si')
      alpha = 37.000000_real64
      c6 = 305.000000_real64
      r0 = 4.200000_real64

    case ('P')
      alpha = 25.000000_real64
      c6 = 185.000000_real64
      r0 = 4.010000_real64

    case ('S')
      alpha = 19.600000_real64
      c6 = 134.000000_real64
      r0 = 3.860000_real64

    case ('Cl')
      alpha = 15.000000_real64
      c6 = 94.600000_real64
      r0 = 3.710000_real64

    case ('Ar')
      alpha = 11.100000_real64
      c6 = 64.300000_real64
      r0 = 3.550000_real64

    case ('K')
      alpha = 292.900000_real64
      c6 = 3897.000000_real64
      r0 = 3.710000_real64

    case ('Ca')
      alpha = 160.000000_real64
      c6 = 2221.000000_real64
      r0 = 4.650000_real64

    case ('Sc')
      alpha = 120.000000_real64
      c6 = 1383.000000_real64
      r0 = 4.590000_real64

    case ('Ti')
      alpha = 98.000000_real64
      c6 = 1044.000000_real64
      r0 = 4.510000_real64

    case ('V')
      alpha = 84.000000_real64
      c6 = 832.000000_real64
      r0 = 4.440000_real64

    case ('Cr')
      alpha = 78.000000_real64
      c6 = 602.000000_real64
      r0 = 3.990000_real64

    case ('Mn')
      alpha = 63.000000_real64
      c6 = 552.000000_real64
      r0 = 3.970000_real64

    case ('Fe')
      alpha = 56.000000_real64
      c6 = 482.000000_real64
      r0 = 4.230000_real64

    case ('Co')
      alpha = 50.000000_real64
      c6 = 408.000000_real64
      r0 = 4.180000_real64

    case ('Ni')
      alpha = 48.000000_real64
      c6 = 373.000000_real64
      r0 = 3.820000_real64

    case ('Cu')
      alpha = 42.000000_real64
      c6 = 253.000000_real64
      r0 = 3.760000_real64

    case ('Zn')
      alpha = 40.000000_real64
      c6 = 284.000000_real64
      r0 = 4.020000_real64

    case ('Ga')
      alpha = 60.000000_real64
      c6 = 498.000000_real64
      r0 = 4.190000_real64

    case ('Ge')
      alpha = 41.000000_real64
      c6 = 354.000000_real64
      r0 = 4.200000_real64

    case ('As')
      alpha = 29.000000_real64
      c6 = 246.000000_real64
      r0 = 4.110000_real64

    case ('Se')
      alpha = 25.000000_real64
      c6 = 210.000000_real64
      r0 = 4.040000_real64

    case ('Br')
      alpha = 20.000000_real64
      c6 = 162.000000_real64
      r0 = 3.930000_real64

    case ('Kr')
      alpha = 16.800000_real64
      c6 = 129.600000_real64
      r0 = 3.820000_real64

    case ('Rb')
      alpha = 319.200000_real64
      c6 = 4691.000000_real64
      r0 = 3.720000_real64

    case ('Sr')
      alpha = 199.000000_real64
      c6 = 3170.000000_real64
      r0 = 4.540000_real64

    case ('Rh')
      alpha = 56.1_real64
      c6 = 469.0_real64
      r0 = 3.95_real64

    case ('Pd')
      alpha = 23.680000_real64
      c6 = 157.500000_real64
      r0 = 3.66000_real64

    case ('Ag')
      alpha = 50.600000_real64
      c6 = 339.000000_real64
      r0 = 3.820000_real64

    case ('Cd')
      alpha = 39.7_real64
      c6 = 452.0_real64
      r0 = 3.99_real64

    case ('Te')
      alpha = 37.65_real64
      c6 = 396.0_real64
      r0 = 4.22_real64

    case ('I')
      alpha = 35.000000_real64
      c6 = 385.000000_real64
      r0 = 4.170000_real64

    case ('Xe')
      alpha = 27.300000_real64
      c6 = 285.900000_real64
      r0 = 4.080000_real64

    case ('Ba')
      alpha = 275.0_real64
      c6 = 5727.0_real64
      r0 = 4.77_real64

    case ('Ir')
      alpha = 42.51_real64
      c6 = 359.1_real64
      r0 = 4.00_real64

    case ('Pt')
      alpha = 39.68_real64
      c6 = 347.1_real64
      r0 = 3.92_real64

    case ('Au')
      alpha = 36.5_real64
      c6 = 298.0_real64
      r0 = 3.86_real64

    case ('Hg')
      alpha = 33.9_real64
      c6 = 392.0_real64
      r0 = 3.98_real64

    case ('Pb')
      alpha = 61.8_real64
      c6 = 697.0_real64
      r0 = 4.31_real64

    case ('Bi')
      alpha = 49.02_real64
      c6 = 571.0_real64
      r0 = 4.32_real64

    case default

      call messages_write('vdw ts: reference free atom parameters not available for species '//trim(atom))
      call messages_fatal(namespace=namespace)

    end select

    POP_SUB(get_vdw_param)
  end subroutine get_vdw_param

end module vdw_ts_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
