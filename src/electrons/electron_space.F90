!! Copyright (C) 2002-2006 M. Marques, A. Castro, A. Rubio, G. Bertsch
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module electron_space_oct_m
  use debug_oct_m
  use global_oct_m
  use messages_oct_m
  use namespace_oct_m
  use parser_oct_m
  use profiling_oct_m
  use space_oct_m
  use varinfo_oct_m

  implicit none

  private

  public ::                   &
    electron_space_t

  !> Extension of space that contains the knowledge of the spin dimension.
  type, extends(space_t) :: electron_space_t

    integer :: ispin !< Spin component: unpolarized, spin-polarized, spinor.

  end type electron_space_t

  !> Parameters...
  integer, public, parameter :: &
    UNPOLARIZED    = 1,         &
    SPIN_POLARIZED = 2,         &
    SPINORS        = 3

  interface electron_space_t
    procedure electron_space_constructor
  end interface electron_space_t


contains

  !----------------------------------------------------------
  function electron_space_constructor(namespace) result(this)
    type(electron_space_t)           :: this
    type(namespace_t),  intent(in)   :: namespace

    PUSH_SUB(electron_space_constructor)

    ! Initialize first an euclidian space
    this%space_t = space_t(namespace)

    !%Variable SpinComponents
    !%Type integer
    !%Default unpolarized
    !%Section States
    !%Description
    !% The calculations may be done in three different ways: spin-restricted (TD)DFT (<i>i.e.</i>, doubly
    !% occupied "closed shells"), spin-unrestricted or "spin-polarized" (TD)DFT (<i>i.e.</i> we have two
    !% electronic systems, one with spin up and one with spin down), or making use of two-component
    !% spinors.
    !%Option unpolarized 1
    !% Spin-restricted calculations.
    !%Option polarized 2
    !%Option spin_polarized 2
    !% (Synonym <tt>polarized</tt>.) Spin-unrestricted, also known as spin-DFT, SDFT. This mode will double the number of
    !% wavefunctions necessary for a spin-unpolarized calculation.
    !%Option non_collinear 3
    !%Option spinors 3
    !% (Synonym: <tt>non_collinear</tt>.) The spin-orbitals are two-component spinors. This effectively allows the spin-density to
    !% be oriented non-collinearly: <i>i.e.</i> the magnetization vector is allowed to take different
    !% directions at different points. This vector is always in 3D regardless of <tt>Dimensions</tt>.
    !%End
    call parse_variable(namespace, 'SpinComponents', UNPOLARIZED, this%ispin)
    if (.not.varinfo_valid_option('SpinComponents', this%ispin)) call messages_input_error(namespace, 'SpinComponents')
    call messages_print_var_option('SpinComponents', this%ispin, namespace=namespace)

    POP_SUB(electron_space_constructor)
  end function electron_space_constructor

end module electron_space_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:

