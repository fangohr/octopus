---
title: "oct-spin_susceptibility"
#series: "Manual"
---


### NAME
oct-spin_susceptibility - Compute the spin susceptibility for magnon calculations

### DESCRIPTION
This program is one of the {{< octopus >}} utilities.

Its behavior is explained in [Magnon tutorial](../../../tutorial/unsorted/magnons). 

---------------------------------------------
