!! Copyright (C) 2023 M. Oliveira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module clock_oct_m
  use, intrinsic :: iso_fortran_env
  use global_oct_m
  use iteration_counter_oct_m

  implicit none

  private

  public :: clock_t, &
    CLOCK_MINIMUM_DT

  !> This CLOCK_MINIMUM_DT parameter defines what is the time-step in the common
  !! reference frame of all clocks. It means that all the time-steps must be
  !! multiples of this one. This parameter also controls what is the maximum
  !! time a clock can have:
  !!
  !!   maximum time = huge(iteration)*CLOCK_MINIMUM_DT
  !!
  !! where "iteration" is the iteration counter in the common reference frame
  !! from iteration_counter_t.
  !!
  !! For 64 bit signed integers, huge(iteration) = 9223372036854775807, and if
  !! CLOCK_MINIMUM_DT = 1.0e-13, then one gets
  !!
  !!   maximum time = 922337.20368547761
  !!
  !! If times are given in atomic units of time, then
  !!
  !!   maximum time ~= 22310 femtosecond
  !!
  FLOAT, parameter :: CLOCK_MINIMUM_DT = 1.0e-13_real64

  interface clock_t
    module procedure clock_constructor
  end interface clock_t

contains

  ! ---------------------------------------------------------
  !> @brief Initialize the clock with a given time step
  !!
  !! The internal clock counter starts at zero or if the optional argument
  !! initial_value is given at the value of initial_value.
  !!
  !! Note that the time step used by the clock might not be identical to the
  !! requested time step. This is because we need to enforce the time step to be
  !! a multiple of the minimum time step or otherwise the calculated time will
  !! not be an exact multiple of the time step.
  type(iteration_counter_t) pure function clock_constructor(time_step, initial_iteration) result(clock)
    FLOAT,   optional, intent(in) :: time_step
    integer, optional, intent(in) :: initial_iteration

    if (present(initial_iteration)) then
      clock%iteration = initial_iteration
    end if
    if (present(time_step)) then
      if (time_step > M_ZERO) then
        clock%step = int(time_step/CLOCK_MINIMUM_DT, int64)
      else
        clock%step = 1
      end if
    end if
    clock%global_iteration = clock%step * clock%iteration

    clock%value => clock_time

  end function clock_constructor

  ! ---------------------------------------------------------
  pure FLOAT function clock_time(this)
    class(iteration_counter_t), intent(in) :: this

    clock_time = this%global_iteration * CLOCK_MINIMUM_DT

  end function clock_time

end module clock_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
