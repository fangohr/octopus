 #!/bin/bash
module load MKL
module load GSL

export PREFIX=$HOME/Software
export FFTW3_HOME=$HOME/local/fftw-3.3.2
export PFFT_HOME=$HOME/local/pfft-1.0.5-alpha

export MKL_LIBS=$MKL_HOME/lib/intel64
export MKLROOT=$MKL_HOME
export MKL_LIBS="-L$MKLROOT/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lmkl_blacs_intelmpi_lp64 -lpthread -lm"
export CC=mpicc
export CFLAGS="-xHost -O3 -m64 -ip -prec-div -static-intel -sox -I$ISF_HOME/include -openmp -I$MKLROOT/include"
export FC=mpif90
export FCFLAGS="-xHost -O3 -m64 -ip -prec-div -static-intel -sox -I$FFTW3_HOME/include -openmp -I$ISF_HOME/include -I$MKLROOT/include"

export LIBS_BLAS="${MKL_LIBS}"
export LIBS_FFT="-Wl,--start-group -L$PFFT_HOME/lib -L$FFTW3_HOME/lib -lpfft -lfftw3 -lfftw3_mpi -Wl,--end-group"


../configure --prefix=$PREFIX/octopus/ \
--enable-openmp --with-libxc-prefix=$PREFIX/libxc \
--disable-gdlib --with-gsl-prefix=/apps/GSL/1.15 \
--enable-mpi --enable-newuoa \
--with-pfft-prefix=$PFFT_HOME
