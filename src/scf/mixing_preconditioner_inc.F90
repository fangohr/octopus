!! Copyright (C) 2024 A. Buccheri, S. Ohlmann.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

!> @brief Kerker preconditioner for the mixing operator in real space.
!!
!! Computes the preconditioned residual:
!! \f[
!!    f_{out} = (\Delta - q_0^2)^{-1} \Delta \delta f
!! \f]
!! by multiplying through by \f$(\Delta - q_0^2)\f$ and solving the
!! linear system:
!! \f[
!!    (\Delta - q_0^2) f_{out} = \Delta \delta f
!! \f]
!! for \f$f_{out}\f$. The implementation follows [Shiihara et. al. 2008 Modelling Simul. Mater. Sci. Eng. 16 035004]
!! (https://iopscience.iop.org/article/10.1088/0965-0393/16/3/035004/), however unlike the paper, we do not include
!! the mixing coefficient in the preconditioned residual.
!!
!! If the starting density does not already integrate to the fully correct total charge,
!! an additional charge correction treatment is necessary:
!! \f[
!!    f_{out}  = f_{out} + \frac{a}{V} \int_{V} \delta f d\mathbr{r}
!! \f]
!! See [Winkelmann et. al.](https://doi.org/10.1103/PhysRevB.102.195138) for more details.
subroutine X(kerker_preconditioner)(der, q_0, delta_f, f_out)
  type(derivatives_t), intent(in)    :: der            !< Derivatives
  FLOAT,               intent(in)    :: q_0            !< Kerker parameter
  R_TYPE,  contiguous, intent(in)    :: delta_f(:)     !< Residual
  R_TYPE,              intent(out)   :: f_out(:)       !< Preconditioned Residual

  FLOAT,   parameter :: abs_cg_tol = 1.e-10_real64 !< Empirically-determined CG absolute tolerance
  integer, parameter :: max_iter = 500    !< Max number of CG iterations
  integer :: iter                         !< In: Max number of CG iterations to solve for
  !                                              the preconditioned residual
  !                                          Out: Number of iterations used
  R_TYPE :: res                           !< CG residual
  FLOAT  :: cg_threshold                  !< CG convergence threshold
  R_TYPE, allocatable :: tmp(:), lapl(:)  !< Work arrays

  R_TYPE :: delta_f_G0                    !< The G=0 contribution to the residual
  FLOAT  :: volume                        !< System volume

  PUSH_SUB(X(kerker_preconditioner))
  SAFE_ALLOCATE(tmp(der%mesh%np_part))
  SAFE_ALLOCATE(lapl(der%mesh%np_part))

  ! Make the threshold relative to |delta_f|
  cg_threshold = X(mf_nrm2)(der%mesh, delta_f) * abs_cg_tol
  iter = max_iter

  delta_f_G0 = X(mf_integrate)(der%mesh, delta_f)
  volume = der%mesh%volume_element * der%mesh%np

  ! Solve the RHS
  ! Define delta_f
  call lalg_copy(der%mesh%np, delta_f, tmp)
  ! Compute Laplacian of delta_f
  call X(derivatives_lapl)(der, tmp, lapl)
  ! Initial guess for the preconditioned residual
  tmp(1:der%mesh%np) = delta_f(1:der%mesh%np) - delta_f_G0

  ! Solve (\Delta - q_0^2) fout = a \Delta delta_f, for f_out
  call X(conjugate_gradients)(der%mesh%np, tmp, lapl, laplacian_op, X(mf_dotp_aux), iter, &
    residue=res, threshold=cg_threshold)
  f_out(1:der%mesh%np) = tmp(1:der%mesh%np) + delta_f_G0 / volume

  if (debug%info) then
    write(message(1), '(a, i3, a, i3, a)') "Info: Kerker linear solver converged in ", iter, " / ", max_iter, " iterations."
    call messages_info(1)
  end if

  SAFE_DEALLOCATE_A(tmp)
  SAFE_DEALLOCATE_A(lapl)
  POP_SUB(X(kerker_preconditioner))

contains

  ! Computes the LHS of the CG, i.e. Ax
  ! where A = (\Delta - q_0^2) and x = f_out
  subroutine laplacian_op(x, lx)
    R_TYPE,             intent(in )   :: x(:)  !< Current guess for preconditioned residual
    R_TYPE, contiguous, intent(out)   :: lx(:) !< The LHS product of CG: Ax, or (\Delta - q_0^2) f_out

    R_TYPE, allocatable :: tmpx(:)

    SAFE_ALLOCATE(tmpx(der%mesh%np_part))
    call lalg_copy(der%mesh%np, x, tmpx)
    ! Delta f_out
    call X(derivatives_lapl)(der, tmpx, lx)
    ! [Delta f_out - q_0^2 *f_out]
    call lalg_axpy(der%mesh%np, -q_0**2, x, lx)
    SAFE_DEALLOCATE_A(tmpx)

  end subroutine laplacian_op

end subroutine X(kerker_preconditioner)

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
