---
title: "Which quantity can Octopus compute"
#series: "Manual"
weight: 2
---



{{< octopus >}} is a software package for {{< name "density-functional theory" >}} (DFT), and {{< name "time-dependent density functional theory" >}} (TDDFT). As such, it can compute many quantities. These quantities can be integrated quantities, scalar fields, vector fields. Some are accessible for equilibrium (ground-state) calculations, whereas some are only computed for time-dependent calculations.

Below, we list the quantities that can be computed by Octopus and provide relevant equations and references as well as link to details on how to compute these quantities using {{< octopus >}}. 

## Ionic output

### **Geometry**
{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    geometry
  %
  {{< /code-block >}}
{{< /expand >}}

Outputs file containing the coordinates of the atoms treated within quantum mechanics. If {{< variable "OutputFormat">}} = xyz, the file is called geometry.xyz; a file crystal.xyz is written with a supercell geometry if the system is periodic; if point charges were defined in the PDB file (see {{< variable "PDBCoordinates">}}), they will be output in the file geometry_classical.xyz. If {{< variable "OutputFormat">}} = xcrysden, a file called geometry.xsf is written.

### **Forces**
{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    forces
  %
  {{< /code-block >}}
{{< /expand >}}

Outputs file forces.xsf containing structure and forces on the atoms as a vector associated with each atom, which can be visualized with XCrySDen.


## Equilibrium electronic output

This series of output quantities are obtained by specifying the variable block {{< variable "Output">}} in the input file. 
Other ones can be obtained for time-dependent calculations using the variable block {{< variable "TDOutput">}}, see below.

### **Potential**

{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    potential
  %
  {{< /code-block >}}
{{< /expand >}}


outputs Kohn-Sham potential, separated by parts. 
File names are:
* {{< file "v0" >}} for the local part of the ionic potential
* {{< file "vc" >}} for the classical potential (if it exists)
* {{< file "vh" >}} for the Hartree potential
* {{< file "vks" >}} for the local part of the Kohn-Sham potential, 
* {{< file "vxc" >}} for the exchange-correlation potentials. 

For $v_{s}$ and $v_{xc}$, a suffix for spin is added in the spin-polarized case.

It is also possible to output the gradient of the potential using
{{< code-inline >}}{{< variable "Output">}} = potential_gradient{{< /code-inline >}}

### **Electronic density**

{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    density  
  %
  {{< /code-block >}}
{{< /expand >}}


Outputs the electronic density. The output file is called density-, or lr_density- in linear response.

In the polarized case, this is given by
$$ n_\sigma(\vec{r}) = \sum_{i=1}^{N_\sigma} \left| \phi_{i\sigma}(\vec{r}) \right|^2 ,$$

whereas in the noncollinear case, it is
$$ n_{\sigma\sigma'}(\vec{r}) = \sum_{i=1}^{N_\sigma} \phi_{i\sigma}(\vec{r}) \phi^*_{i\sigma'}(\vec{r}) .$$

In the spinor case, four real components are printed, labelled {{< file "sp-1">}} to {{< file "sp-4">}}, corresponding to $n_{\uparrow\uparrow}$, $n_{\downarrow\downarrow}$, $\Re\{n_{\uparrow\downarrow}\}$, and $\Im\{n_{\uparrow\downarrow}\}$.

### **Kinetic energy density**

{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    kinetic_energy_density
  %
  {{< /code-block >}}
{{< /expand >}}

 Outputs kinetic-energy density, defined as:

 $$ \tau_\sigma(\vec{r}) = \sum_{i=1}^{N_\sigma} \sum_{\vec{k}} w_{\vec{k}} \left| \vec{\nabla} \phi_{i\sigma}(\vec{r}) \right|^2  .$$

The index $\sigma$ is the spin index for the spin-polarized case, or if you are using spinors. For spin-unpolarized calculations, you get the total kinetic-energy density. The previous expression assumes full or null occupations. If fractional occupation numbers, each term in the sum is weighted by the occupation. Also, if we are working with an infinite system, all k-points are summed up, with their corresponding weights. The files will be called {{< file "tau-sp1">}} and {{< file "tau-sp2">}}, if the spin-resolved kinetic energy density is produced (runs in spin-polarized and spinors mode), or only tau if the run is in spin-unpolarized mode.

### **Current density**

{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    current
  %
  {{< /code-block >}}
{{< /expand >}}

Outputs the total current density, defined as

$$ \vec{j}(\vec{r}) = \sum_{i=1}^{N} \sum_{\vec{k}} w_{\vec{k}} \Big( \Im \left[ \langle i,\vec{k} | \vec{\nabla} + \frac{|e|}{c} \vec{A}(t) |  i,\vec{k} \rangle \right] + \Re\left[ \langle i,\vec{k} |  e^{-i\frac{|e|}{c}\vec{A}(t)\cdot\vec{\hat{r}}} [\vec{\hat{r}}, \hat{V}_{\rm NL}] e^{i\frac{|e|}{c}\vec{A}(t)\cdot\vec{\hat{r}}} |  i,\vec{k} \rangle \right]  \Big) ,$$

where $\hat{V}_{\rm NL}$ is the nonlocal part of the Hamiltonian (pseudopotential, Fock operator, etc.), and $\vec{A}(t)$ is a potential vector potential used during the simulation.
In order to control if the nonlocal part of the pseudopotential (and other nonlocal operator contributions) are included or not, please refer to the variable {{< variable "CurrentDensity">}}.
Note that other contributions are added for mGGA and hybrid functionals. 

The output file is called {{< file "current-">}}. For linear response, the filename is {{< file "lr_current-">}}.


### **Energy density**

{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    energy_density
  %
  {{< /code-block >}}
{{< /expand >}}


Outputs the total energy density to a file called energy_density. This contains contribution from the kinetic energy density, the external potential, the Hartree, and the $xc$ part. Note that the energy density is only correct for LDA and GGA functionals. For more complex functionals, some contributions might be missing, especially for potential-only functionals which are not derived from an energy functionals.


### **Wavefunctions**

{{< octopus >}} offers different ways to output the wavefunction. This can be on real space grid, in Fourier space, or looking at the density of each individual orbitals. 

{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    wfs
  %
  {{< /code-block >}}
{{< /expand >}}


outputs the wavefunctions on the real-space grid. Which wavefunctions are to be printed is specified by the variable {{< variable "OutputWfsNumber">}}. The output file is called {{< file "wf-">}}, or {{< file "lr_wf-">}} in linear response.

{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    wfs_sqmod
  %
  {{< /code-block >}}
{{< /expand >}}


outputs the modulus squared of the wavefunctions. The output file is called {{< file "sqm-wf-">}}. For linear response, the filename is {{< file "sqm_lr_wf-">}}.

{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    wfs_fourier
  %
  {{< /code-block >}}
{{< /expand >}}



outputs wavefunctions in Fourier space. This output is experimental! This is only implemented for the ETSF file format output. The file will be called {{< file "wfs-pw-etsf.nc">}}.


### **Electron localization function (ELF)**

{{< octopus >}} can compute the electron localization function (ELF). This can be computed both in the static case and in the time-dependent case.
The definition of the time-dependent case can be found in Ref.[{{< article title="Time-dependent electron localization function" authors="Burnus, T. and Marques, M. A. L. and Gross, E. K. U." journal="Phys. Rev. A" volume="71" pages="010501" year="2005" doi="10.1103/PhysRevA.71.010501" >}}].
Note that the images and videos from this paper can be found [here](https://www.tddft.org/td-elf/).

{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    elf
  %
  {{< /code-block >}}
{{< /expand >}}

In the general case, the ELF is defined as:

 $$ f_{\rm ELF}(\vec{r},t) = \frac{1}{1+[D_\sigma(\vec{r},t)/D_\sigma^0(\vec{r},t)]^2} ,$$

where 

 $$D_\sigma^0(\vec{r},t) = \tau_\sigma^{\rm HEG}[n_\sigma(\vec{r},t)] = \frac{3}{5}(6\pi^2)^{2/3}n_\sigma^{5/3}(\vec{r},t),$$

is the kinetic energy density of a homogeneous electron gas of density $n_\sigma$. Here $\sigma$ refers to the spin index.

The function $D_\sigma$ is given in the general case by

 $$ D_\sigma(\vec{r},t) = \tau_\sigma(\vec{r},t) - \frac{1}{4}\frac{[\nabla n_\sigma(\vec{r},t)]^2}{n_\sigma(\vec{r},t)} - \frac{j_\sigma^2(\vec{r},t)}{n_\sigma(\vec{r},t)} ,$$

where the last term is the current term of the time-dependent ELF, that is also present for complex-valued wavefunctions, like in current-carrying ground states.
In order to control the use (or not) of the current term in the ELF, please refer to the variable {{< variable "ELFWithCurrentTerm">}}.

  The output file is called {{< file "elf-">}}, or {{< file "lr_elf-">}} in linear response, in which case the associated function D is also written, as {{< file "lr_elf_D-">}}. Only in 2D and 3D.

{{< octopus >}} can also output the basins of attraction of the ELF.
{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    elf_basins
  %
  {{< /code-block >}}
{{< /expand >}}
  The output file is called {{< file "elf_rs_basins.info">}}. Only in 2D and 3D.

### **Bader population analysis**
{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    bader
  %
  {{< /code-block >}}
{{< /expand >}}

Outputs Laplacian of the density which shows lone pairs, bonded charge concentrations and regions subject to electrophilic or nucleophilic attack. See RF Bader, Atoms in Molecules: A Quantum Theory (Oxford Univ. Press, Oxford, 1990).
The corresponding basins are also outputed as {{<file "bader_basins">}}.

### **Electronic pressure**
{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    el_pressure
  %
  {{< /code-block >}}
{{< /expand >}}

It is possible to output electronic pressure, as defined [{{< article title="Quantum Stress Focusing in Descriptive Chemistry" authors="Tao, Jianmin and Vignale, Giovanni and Tokatly, I. V." journal="Phys. Rev. Lett." volume="100" pages="206405" year="2008" doi="10.1103/PhysRevLett.100.206405" >}}].

To be more precise, what the code outputs is the function

$$L(\vec{r}) = \frac{1}{2}\Big(1+\frac{\tilde{p}}{\sqrt{1+\tilde{p}^2}} \Big)  ,$$

where we defined here $\tilde{p} = (p^{\rm S}+p^{\rm xc})/p^{\rm TF}$, with the independent particle pressure $p^{\rm S}$ given by

$$p^{\rm S}(\vec{r}) = \frac{1}{3}\tau(\vec{r}) - \frac{1}{4} \nabla^2 n(\vec{r}) ,$$

and the exchange-correlation pressure $p^{\rm xc}$ given by 
 
$$p^{\rm xc}(\vec{r}) = n(\vec{r})v_{\rm xc}(\vec{r}) - e_{\rm xc}(\vec{r}),$$

where $e_{\rm xc}(\vec{r})$ is the exchange-correlation energy density.

Please note that the original paper does not include the exchange-correlation pressure in the definition of $L(\vec{r})$. Moreover, this quantity is only valid for spherically-symmetric atoms.
At the moment, this is only available for spin-unpolarized calculations and for the Kohn-Sham DFT theory level . Also, if the functional does not have an energy, this won't work.

### **Matrix elements**

Outputs can output a series of matrix elements from the electronic states. 
What is output can be controlled by the {{< variable "OutputMatrixElements">}} variable, which needs to be specified in the input file.

{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    matrix_elements
  %

  %{{< variable "OutputMatrixElements">}}
    momentum
    ks_multipoles
  %
  {{< /code-block >}}
{{< /expand >}}

At the moment, the code support the following options for {{< variable "OutputMatrixElements">}}:
 - momentum
 - ang_momentum
 - one_body
 - two_body
 - two_body_exc_k
 - ks_multipoles
 - dipole
Please refer to the variable description of {{< variable "OutputMatrixElements">}} for more details.

Octopus can also output transition-potential approximation (TPA) matrix elements, using $\vec{q}$-vector specified by {{< variable "MomentumTransfer">}}.

{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    tpa
  %
  {{< /code-block >}}
{{< /expand >}}

### **Density of states**
{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    dos
  %
  {{< /code-block >}}
{{< /expand >}}
Outputs density of states. See {{< variable "DOSEnergyMax">}}, {{< variable "DOSEnergyMin">}}, {{< variable "DOSEnergyPoints">}}, and {{< variable "DOSGamma">}}. 

### **Dipole-moment, dipole-moment density and Polarizability density**

Octopus can compute ground-state and excited dipoles. The ground-state dipole moment is computed by default at the end of a ground-state calculation, see {{< variable "SCFCalculateDipole">}}. The values are written in the {{<file "static/info">}} file.

The excited-state dipoles can be obtained using the Casida/Petersilka equation, see the corresponding tutorial for more details.

Outputs dipole-moment density {{< file "dipole_density-">}}, or polarizability density {{< file "alpha_density-">}} in linear response.
If {{< variable "ResponseMethod">}} = finite_differences, the hyperpolarizability density {{< file "beta_density-">}} is also printed.

{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    pol_density
  %
  {{< /code-block >}}
{{< /expand >}}


### **Grid-point coordinates**
Outputs values of the coordinates over the grid.
Files will be called {{<file "mesh_r-">}} followed by the direction.

{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    mesh_r
  %
  {{< /code-block >}}
{{< /expand >}}


### **Exchange-correlation density**
Outputs the xc density, which is defined as the charge density that generates the XC potential, i.e., $-1/4\pi$ times the Laplacian of the XC potential. 
The files are called {{< file "nxc">}}.

{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    xc_density
  %
  {{< /code-block >}}
{{< /expand >}}


### **Heat current density**
Outputs the total heat current density. 
The output file is called {{<file "heat_current-">}}.

{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    heat_current
  %
  {{< /code-block >}}
{{< /expand >}}


### **Electron-photon correlation function**
Outputs the electron-photon correlation function. 
The output file is called {{<file "photon_correlator">}}.

{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    photon_correlator
  %
  {{< /code-block >}}
{{< /expand >}}


### **J_flow**
todo: document J_flow option!

### **Exchange correlation torque**

{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    xc_torque
  %
  {{< /code-block >}}
{{< /expand >}}

Outputs the exchange-correlation torque. This output is only possible for the spinor case and in the 3D case.
$$ \vec{t}(\vec{r}) =  \vec{m}(\vec{r})\times \vec{B}_{\rm xc}(\vec{r})   ,$$

where $\vec{m}$ is the magnetization vector, and $\mathbf{B}_{\rm xc}$ is the exchange-correlation magnetic field.


### Brillouin-zone resolved outputs

It is also possible to output certain quantities resolved in momentum space, for periodic systems.
The quantities are obtained on the k-point grid used to sample the Brillouin zone.
At the moment, only specific output formats are possible.

#### **current_kpt**
Outputs the total current density resolved in momentum space. The output file is called {{<file "current_kpt-" >}}.

#### **density_kpt**
Outputs the electronic density resolved in momentum space.

#### **eigenval_kpt**
Outputs the eigenvalues resolved in momentum space, with one file for each band. 


## Time-dependent outputs
This series of output quantities are obtained by specifying the variable block {{< variable "TDOutput">}} in the input files.


### **multipoles**
Outputs the (electric) multipole moments of the density to the file {{< file "td.general/multipoles" >}}. This is required to, e.g., calculate optical absorption spectra of finite systems. The maximum value of lll can be set with the variable {{< variable "TDMultipoleLmax">}}.

### **angular**
Outputs the orbital angular momentum of the system to {{< file "td.general/angular" >}}, which can be used to calculate circular dichroism.

### **spin**
(Experimental) Outputs the expectation value of the spin, which can be used to calculate magnetic circular dichroism.

### **populations**
(Experimental) Outputs the projection of the time-dependent Kohn-Sham Slater determinant onto the ground state (or approximations to the excited states) to the file {{<file "td.general/populations">}}. Note that the calculation of populations is expensive in memory and computer time, so it should only be used if it is really needed. See {{< variable "TDExcitedStatesToProject">}}.

### **geometry**
If set (and if the atoms are allowed to move), outputs the coordinates, velocities, and forces of the atoms to the the file {{<file "td.general/coordinates">}}. 
On by default if {{<code-inline>}}{{<variable "MoveIons">}} = yes {{< /code-inline >}}.

### **dipole_acceleration**
When set, outputs the acceleration of the electronic dipole, calculated from the Ehrenfest theorem, in the file {{<file "td.general/acceleration">}}. This file can then be processed by the utility {{<file "oct-harmonic-spectrum">}} in order to obtain the harmonic spectrum.

### **laser**
If set, outputs the laser field to the file {{<file "td.general/laser">}}. On by default if {{<variable "TDExternalFields">}} is set.

### **energy**
If set, octopus outputs the different components of the energy to the file {{<file "td.general/energy">}}. Will be zero except for every {{<variable "TDEnergyUpdateIter">}} iterations.

### **td_occup**
(Experimental) If set, outputs the projections of the time-dependent Kohn-Sham wavefunctions onto the static (zero-time) wavefunctions to the file {{<file "td.general/projections.XXX">}}. Only use this option if you really need it, as it might be computationally expensive. See {{< variable "TDProjStateStart">}}. The output interval of this quantity is controled by the variable {{< variable "TDOutputComputeInterval">}} In case of states parallelization, all the ground-state states are stored by each task.

### **local_mag_moments**
If set, outputs the local magnetic moments, integrated in sphere centered around each atom. The radius of the sphere can be set with {{< variable "LocalMagneticMomentsSphereRadius">}}.

### **gauge_field**
If set, outputs the vector gauge field corresponding to a spatially uniform (but time-dependent) external electrical potential. This is only useful in a time-dependent periodic run. On by default if {{< variable "GaugeVectorField" >}} is set.

### **temperature**
If set, the ionic temperature at each step is printed. On by default if {{<code-inline>}}{{<variable "MoveIons">}} = yes{{</code-inline>}}.

### **ftchd**
Write Fourier transform of the electron density to the file ftchd.X, where X depends on the kick (e.g. with sin-shaped perturbation X=sin). This is needed for calculating the dynamic structure factor. In the case that the kick mode is qbessel, the written quantity is integral over density, multiplied by spherical Bessel function times real spherical harmonic. On by default if {{< variable "TDMomentumTransfer">}} is set.

### **dipole_velocity**
When set, outputs the dipole velocity, calculated from the Ehrenfest theorem, in the file {{<file "td.general/velocity">}}. This file can then be processed by the utility {{<file "oct-harmonic-spectrum">}} in order to obtain the harmonic spectrum.

### **eigenvalues**
Write the KS eigenvalues.

### **ionization_channels**
Write the multiple-ionization channels using the KS orbital densities as proposed in C. Ullrich, Journal of Molecular Structure: THEOCHEM 501, 315 (2000).

### **total_current**
Output the total current (average of the current density over the cell).

### **partial_charges**
Bader and Hirshfeld partial charges. The output file is called {{<file "td.general/partial_charges">}}.

### **td_kpoint_occup**
Project propagated Kohn-Sham states to the states at t=0 given in the directory restart_proj (see %RestartOptions). This is an alternative to the option td_occup, with a formating more suitable for k-points and works only in k- and/or state parallelization

### **td_floquet**
Compute non-interacting Floquet bandstructure according to further options: {{< variable "TDFloquetFrequency">}}, {{< variable "TDFloquetSample">}}, {{< variable "TDFloquetDimension">}}. This is done only once per td-run at t=0. works only in k- and/or state parallelization

### **n_excited_el**
Output the number of excited electrons, based on the projections of the time evolved wave-functions on the ground-state wave-functions. The output interval of this quantity is controled by the variable {{< variable "TDOutputComputeInterval">}}

### **coordinates_sep**
Writes geometries in a separate file.

### **velocities_sep**
Writes velocities in a separate file.

### **forces_sep**
Writes forces in a separate file.

### **total_heat_current**
Output the total heat current (average of the heat current density over the cell).

### **total_magnetization**
Writes the total magnetization, where the total magnetization is calculated at the momentum defined by {{< variable "TDMomentumTransfer">}}. This is used to extract the magnon frequency in case of a magnon kick.

### **photons_q**
Writes photons_q in a separate file. 

### Photoemission spectroscopy (PES) outputs

#### **PES_wfs**
Outputs the photoelectron wavefunctions. The file name is {{< file "pes_wfs-" >}} plus the orbital number.

#### **PES_density**
Outputs the photolectron density. Output file is {{<file "pes_dens-" >}} plus spin species if spin-polarized calculation is performed.

#### **PES**
Outputs the time-dependent photoelectron spectrum.


## Specialized outputs
### DFT+U(+V) outputs
The following outputs are only available for DFT+U(+V) calculations.

#### **Occupation matrices**
Outputs the occupation matrices of LDA+U.
{{< expand "Input example:">}}
  {{< code-block >}}
  %{{< variable "Output">}}
    occ_matrices
  %
  {{< /code-block >}}
{{< /expand >}}

In the case of a periodic system, the matrix elemets of the occupation matrices $n^{I,n,l,\sigma}\_{mm'}$ are given by

$$
n^{I,n,l,\sigma}\_{mm'} = \sum_{n}\sum\_{\mathbf{k}}^{\mathrm{BZ}} w_\mathbf{k}f\_{n\mathbf{k}}^\sigma \langle\psi\_{n,\mathbf{k}}^{\sigma} |\hat{P}^{I,n,l}\_{mm'}|\psi\_{n,\mathbf{k}}^{\sigma} \rangle , 
$$

where $w_{\mathbf{k}}$ is the $\mathbf{k}$-point weight and $f_{n\mathbf{k}}^\sigma$ is the occupation of the Bloch state $|\psi\_{n,\mathbf{k}}^{\sigma} \rangle$.  
Here, $| \phi^{I,n,l}\_{m}\rangle$ are the localized orbitals that form the basis used to describe electron localization and $\hat{P}^{I,n,l}\_{mm'}$ is the projector associated with these orbitals, usually defined as $\hat{P}^{I,n,l}\_{mm'}=| \phi^{I,n,l}\_{m}\rangle  \langle \phi^{I,n,l}\_{m'}|$.

#### **effectiveU**
If one uses the ACBN0 functional, one can output the Hubbard U computed by {{< octopus >}}.
Outputs the value of the effectiveU for each atoms.

#### **magnetization**
Outputs file containing structure and magnetization of the localized subspace on the atoms as a vector associated with each atom, which can be visualized. For the moment, it only works if a +U is added on one type of orbital per atom.

#### **local_orbitals**
Outputs the localized orbitals that form the correlated subspace.

#### **kanamoriU**
Outputs the Kanamori interaction parameters U, U', and J. These parameters are not determined self-consistently, but are taken from the occupation matrices and Coulomb integrals comming from a standard +U calculation.


### ModelMB outputs

#### **mmb_wfs**
Triggers the ModelMB wavefunctions to be output for each state.

#### **mmb_den**
Triggers the ModelMB density matrix to be output for each state, and the particles specified by the {{< variable "DensitytoCalc">}} block.
Calculates, and outputs, the reduced density matrix. For the moment the trace is made over the second dimension, and the code is limited to 2D. The idea is to model N particles in 1D as an N-dimensional non-interacting problem, then to trace out N-1 coordinates.

### External libraries

#### **BerkeleyGW**
Output for a run with BerkeleyGW. See Output::BerkeleyGW for further specification.
delta_perturbation:
Outputs the "kick", or time-delta perturbation applied to compute optical response in real time.
external_td_potential:
Outputs the (scalar) time-dependent potential.

