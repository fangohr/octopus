set pm3d
set view map
set palette defined (-0.05 "blue", 0 "white", 0.05"red")
set term png size 1000,500

unset surface
unset key

set output 'plot1.png'
set pm3d
set view map
set palette defined (-0.05 "blue", 0 "white", 0.05"red")
set term png size 1000,500

unset surface
unset key

set output 'tutorial_03.1-plot1.png'

set xlabel 'x-direction'
set ylabel 'y-direction'
set cbrange [-0.05:0.05]

set multiplot

set origin 0.025,0
set size 0.45,0.9
set size square
set title 'Electric field E_z (t=0.052664 au)'
sp [-15:15][-15:15] 'Maxwell/output_iter/td.0000025/e_field-z.z=0' u 1:2:3

set origin 0.525,0
set size 0.45,0.9
set size square
set title 'Electric field E_z (t=0.105328 au)'
sp [-15:15][-15:15] 'Maxwell/output_iter/td.0000050/e_field-z.z=0' u 1:2:3

unset multiplot

set output 'tutorial_03.1-plot2.png'

set multiplot

set origin 0.025,0
set size 0.45,0.9
set size square
set title 'Electric field E_z (t=0.157992 au)'
sp [-15:15][-15:15] 'Maxwell/output_iter/td.0000075/e_field-z.z=0' u 1:2:3

set origin 0.525,0
set size 0.45,0.9
set size square
set title 'Electric field E_z (t=0.210656 au)'
sp [-15:15][-15:15] 'Maxwell/output_iter/td.0000100/e_field-z.z=0' u 1:2:3

